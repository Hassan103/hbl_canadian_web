<?php  $page_class = "aboutus_page"; $page_bread = "About <span>Us</span>" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;    ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="about_page_heading">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="panel">

                                    <div class="about_top_text d-flex flex-wrap">
                                        <div class="col-lg-3">
                                            <div class="about_heading_text">
                                                <h1>HCB ​</h1>
                                                <p>is committed to make banking more rewarding for you in Canada.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="about_text">
                                                <p>Habib Canadian Bank (HCB), a wholly owned subsidiary of Habib Bank AG Zurich, began its operations from Mississauga on March 22, 2001. HCB is a member of the Canadian Deposit Insurance Corporation.</p>

                                                <p> Habib Canadian Bank places a high emphasis on personal service. The Bank provides a comprehensive range of commercial and personal banking services and products designed to cover the needs of all its customers from small saver to the larger international corporations.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about_heading text-center">
                                        <h1>Our Vision</h1>
                                    </div>
                                    <div class="panel-text">
                                        <p class="text-center">
                                            To be the most respected financial institution based on trust, service and commitment.
                                        </p>
                                    </div>

                                </div>

                                <div class="panel">
                                    <div class="about_heading text-center">
                                        <h1>The Bank</h1>
                                    </div>
                                    <div class="panel-text">
                                        <p class="">Habib Bank AG Zurich (HBZ) was established in Switzerland in 1967 on a solid foundation of banking tradition that spans several generations. Traditional banking values set in the context of international banking has determined HBZ’s corporate philosophy – “Service with Security” – for over 50 years in operation. Providing corporate, personal, private, and correspondent banking products, we offer highly personalized service through our international network of branches, subsidiaries and affiliates. All group banking functions are managed out of the Head Office located in Zurich, Switzerland at 59 Weinbergstrasse. The group Private Banking division is based nearby at 53a Weinbergstrasse. HBZ, a technology pioneer and leader in the banking sector offers a wide range of financial products and services complemented by innovative delivery channels.</p>
                                        <p class="">Providing corporate, personal, private, and correspondent banking products, we offer highly personalized service through our international network of branches, subsidiaries and affiliates. All group banking functions are managed out of the Head Office located in Zurich, Switzerland at 59 Weinbergstrasse.</p>
                                        <p class="">The group Private Banking division is based nearby at 53a Weinbergstrasse. HBZ, a technology pioneer and leader in the banking sector offers a wide range of financial products and services complemented by innovative delivery channels.</p>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="about_heading text-center">
                                        <h1>Our Motto – Service With Security</h1>
                                    </div>
                                    <div class="panel-text">
                                        <p class="">HBZ is the heir to a rich tradition of commerce and banking dating back to 1841. The international banking and financial community respects HBZ for its conservative approach and its emphasis on high liquidity. The excellent reputation that the bank enjoys today is mainly due to its long tradition, international experience and a team of dedicated professionals who offer personal, friendly and efficient service. As a Swiss incorporated bank, we offer a high level of confidentiality and strict adherence to the rules and regulations of the Swiss Financial Market Supervisory Authority (FINMA) and several other international banking regulators. Today, with over 90 years of banking tradition, HBZ is positioned as a leading international bank, providing business and personal financial services, with a focus on owner-operated enterprises across the globe.</p>
                                        <p class="">Providing corporate, personal, private, and correspondent banking products, we offer highly personalized service through our international network of branches, subsidiaries and affiliates. All group banking functions are managed out of the Head Office located in Zurich, Switzerland at 59 Weinbergstrasse.</p>
                                        <p class="">The group Private Banking division is based nearby at 53a Weinbergstrasse. HBZ, a technology pioneer and leader in the banking sector offers a wide range of financial products and services complemented by innovative delivery channels.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ready-start">
                    <div class="ready_start_text">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <h1>READY TO GET STARTED?</h1>
                                </div>
                                <div class="col-md-4 text-md-right">
                                    <a href="javascript:void(0)" class="common_btn btn_green-light">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Boards of Directors -->

                <div class="board_of_directors">
                    <div class="container">
                        <div class="row">
                            <h1 class="board_heading">
        Board of Directors
        </h1>
                        </div>
                        <div class="row">
                            <div class="d-flex flex-md-wrap horizontal"> 
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
 <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="director_box">
                                    <h5>MUHAMMAD H. HABIB</h5>
                                    <h4>Chairman</h4>
                                    <img src="assets/images/placeholder.png">
                                    <p> <b> President</b></p>
                                    <p> Habib Bank AG Zurich</p>
                                </div>
                            </div>
                        </div>
                        </div>

                      
                    </div>
                </div>

                <section class="time_line">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="time_line_head text-center">
                                    HBZ Timeline
                                </div>

                                <div class="time_line_slider">

                                    <div class="item">
                                        <div class="time_line_Pointer">
                                            <ul>
                                                <li data-tab="1" class="active">
                                                    <button> 1891</button>
                                                </li>
                                                <li data-tab="2">
                                                    <button> 1904</button>
                                                </li>
                                                <li data-tab="3">
                                                    <button> 1912-1921</button>
                                                </li>
                                                <li data-tab="4">
                                                    <button> 1941-1947</button>
                                                </li>
                                                <li data-tab="5">
                                                    <button> 1967</button>
                                                </li>

                                            </ul>
                                        </div>

                                        <div class="tabs">
                                            <div id="tab-1" class="commont_tab active">
                                                <p>
                                                    Habib Esmail, Founder
                                                    <br> At the age of 13, Habib began his working life by joining his uncle's firm ; Khoja Mithabani Nathoo, a leading trader and manufacturer of metals. He became a Partner at the age of 18 and President of the Copper and Brass Merchant's Association.
                                                </p>
                                            </div>
                                            <div id="tab-2" class="commont_tab">
                                                <p>
                                                    Mohamedali Habib, the Builder, the Philanthropist.
                                                    <br> Born on 15 May 1904 in Bombay, the third son of Habib Ismail was endowed with exceptional qualities of leadership and tremendous vision and foresight. He was destined to play the most important role in establishing the “House Of Habib” and founded “Habib Bank Ltd” in August 1941.
                                                </p>
                                            </div>

                                            <div id="tab-3" class="commont_tab">
                                                <p>
                                                    Habib looked to Europe, where he opened offices in Genoa and Vienna, and initiated business relationships with Japan and China importing yarn, glassware and cutlery. Habib & Sons Ltd was founded in 1921, after Habib Esmail’s sons joined him. The firm expanded rapidly with trading and merchant banking at its core.
                                                </p>
                                            </div>
                                            <div id="tab-4" class="commont_tab">
                                                <p>
                                                    Habib Bank Limited
                                                    <br> Habib Bank Limited (HBL) was founded in Bombay, India in 1941 as a public limited company, with a paid up capital of Rs. 2.5 Million. The bank continued to expand its network with a focus on the subcontinent’s major cities. The bank grew to become the flagship of the family. HBL moved its headquarters to Karachi. With a network of branches throughout the subcontinent, the bank went on to play a pivotal role in providing Pakistan’s basic banking and financial needs.
                                                </p>
                                            </div>
                                            <div id="tab-5" class="commont_tab">
                                                <p>
                                                    Habib Bank AG Zurich
                                                    <br> Habib Bank AG Zurich (HBZ) was founded in Zurich, Switzerland on 25 August 1967. Founded by Mohamedali Habib’s son, Hyder Habib. Its first offce was located at no. 1 Tödistrasse, Zurich.
                                                </p>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="item">
                                        <div class="time_line_Pointer">
                                            <ul>
                                                <li data-tab="6">
                                                    <button> 1974</button>
                                                </li>
                                                <li data-tab="7">
                                                    <button> 1978</button>
                                                </li>
                                                <li data-tab="8">
                                                    <button> 1978</button>
                                                </li>
                                                <li data-tab="9">
                                                    <button> 1982</button>
                                                </li>
                                                <li data-tab="10">
                                                    <button> 1989</button>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tabs">
                                            <div id="tab-6" class="commont_tab">
                                                <p>1st January 1974 saw the nationalisation of Pakistan’s banking industry. Under the leadership of Mr. H.M. Habib & Mr. H.D. Habib, HBZ opens international branches in UAE, UK and Oman.</p>
                                            </div>
                                            <div id="tab-7" class="commont_tab">
                                                <p>HBZ expands its international network to Kenya and opened more branches in UAE and UK.</p>
                                            </div>
                                            <div id="tab-8" class="commont_tab">
                                                <p>HBZ Finance Limited is incorporated in Hong Kong, a Deposit Taking Company (DTC) .</p>
                                            </div>
                                            <div id="tab-9" class="commont_tab">
                                                <p>After 8 years at Mühlebachstrasse, HBZ moves its head office to 21 Bergstrasse, Zurich.</p>
                                            </div>
                                            <div id="tab-10" class="commont_tab">
                                                <p>On July 1, 1989 HBZ opens a branch in Paksitan.</p>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="item">
                                        <div class="time_line_Pointer">
                                            <ul>

                                                <li data-tab="11">
                                                    <button> 1993</button>
                                                </li>
                                                <li data-tab="12" class="active">
                                                    <button> 1994</button>
                                                </li>
                                                <li data-tab="13">
                                                    <button> 1995</button>
                                                </li>
                                                <li data-tab="14">
                                                    <button> 2000</button>
                                                </li>
                                                <li data-tab="15">
                                                    <button> 2001</button>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tabs">

                                            <div id="tab-11" class="commont_tab">
                                                <p>The worlds’ first intergated Java™ banking solution hPLUS is launched. This solution revolutionises HBZ‘s technology & operational systems. Under the leadership of Reza Habib, HBZ’s IT department starts developing its own banking solution.</p>
                                            </div>
                                            <div id="tab-12" class="commont_tab">
                                                <p>In 1994, HBZ moves to it’s present headquarters located at 59 Weinbergstrasse, Zurich.</p>
                                            </div>
                                            <div id="tab-13" class="commont_tab">
                                                <p>HBZ Bank Limited, South Africa is incorporated in 1995, as a wholly owned subsidiary of HBZ.</p>
                                            </div>
                                            <div id="tab-14" class="commont_tab">
                                                <p>HBZ launches its online and mobile banking platform offering a range of services, one of the first in the region to do so. HBZ joins the ranks among the top1000 world banks at 862. The bank is ranked at 342 in Soundness and 384 in terms of performance. HBZ is also ranked 28th in The Banker’s country ranking for Switzerland.</p>
                                            </div>
                                            <div id="tab-15" class="commont_tab">
                                                <p>Habib Canadian Bank
                                                    <br>Habib Canadian Bank (HCB) is established as a wholly owned subsidiary of HBZ. HBZ becomes the first bank in the Middle East to offer WAP-based banking services.</p>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="item">
                                        <div class="time_line_Pointer">
                                            <ul>

                                                <li data-tab="16">
                                                    <button> 2004</button>
                                                </li>
                                                <li data-tab="17">
                                                    <button> 2005</button>
                                                </li>
                                                <li data-tab="18">
                                                    <button> 2006</button>
                                                </li>
                                                <li data-tab="19">
                                                    <button> 2009</button>
                                                </li>
                                                <li data-tab="20">
                                                    <button> 2013</button>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tabs">

                                            <div id="tab-16" class="commont_tab">
                                                <p>HBZ began Islamic Banking business on 16 February 2004. Today, there are twenty-five dedicated Islamic Banking branches operating under brand ‘Sirat’.</p>
                                            </div>
                                            <div id="tab-17" class="commont_tab">
                                                <p>The Bank wins a prestigious ComputerWorld Award The awards recognise HBZ’s advanced banking systems and processes. HBZ launches HBZtrade, region’s first online securities trading platform for US capital markets, through its online banking service.</p>
                                            </div>
                                            <div id="tab-18" class="commont_tab">
                                                <p>
                                                    Habib Metropolitan Bank
                                                    <br> HBZ’s operation in Pakistan and Metropolitan Bank merge to form Habib Metropolitan Bank (HMB). HMB is a public listed company on the Karachi Stock Exchange. Today, HMB has over 300 branches and is ranked among the top 10 banks in Pakistan.</p>
                                            </div>
                                            <div id="tab-19" class="commont_tab">
                                                <p>HBZ Services FZE LLC, a subsidiary of HBZ, was established in 2009. It is responsible for providing transaction banking services to the bank.</p>
                                            </div>
                                            <div id="tab-20" class="commont_tab">
                                                <p>HBZ UAE Zonal offce and SZR branch are moved to a new location at Umm Al Sheif, Shaikh Zayed Road, Dubai, U.A.E.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="item">
                                        <div class="time_line_Pointer">
                                            <ul>

                                                <li data-tab="21">
                                                    <button> 2014</button>
                                                </li>
                                                <li data-tab="22">
                                                    <button> 2015</button>
                                                </li>
                                                <li data-tab="23">
                                                    <button> 2017</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tabs">

                                            <div id="tab-21" class="commont_tab">
                                                <p>
                                                    Sirat
                                                    <br>“Sirat”, the Group’s Islamic banking brand was launched on 14th of July 2014, which is an innate reflection of the Group's core values and vision.</p>
                                            </div>
                                            <div id="tab-22" class="commont_tab">
                                                <p>HBZ Finance Ltd has been transformed into Habib Bank Zurich (Hong Kong) Limited.
                                                </p>
                                            </div>
                                            <div id="tab-23" class="commont_tab">
                                                <p>HBZ UAE extended it’s banking experience by introducing Islamic Banking - “Sirat” on March 6, 2017. HBZ launched it’s mobile banking application for its customers.</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <div class="join_team">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="join_team_text">
                                <h1>JOIN THE TEAM</h1>
                                <p>We are always on the lookout for talented people to join us.</p>
                                <a href="our-team.php" class="common_btn btn_green">Appy Now</a>
                            </div>
                            <div>

                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="join_team_img">
                                <img src="assets/images/our-team.png" alt="" class="img-fluid" />
                            </div>

                            <div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->
                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>
