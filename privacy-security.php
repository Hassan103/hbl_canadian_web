<?php  $page_class = "privacy_page"; $page_bread = "<span>Privacy </span> & Security" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page privacy_security_page">
                <div class="heaader_inner privacy-security">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="about_page_heading">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="panel">
                                    <div class="about_heading text-center security_head">
                                        <h1>Common Reporting Standard (CRS) and Foreign Account Tax Compliance Act (FATCA)</h1>
                                    </div>
                                </div>

                                <div class="question_answer">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                What is Common Reporting Standard (CRS)?
                                                </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    In October 2014, over 100 countries endorsed the Standard for Automatic Exchange of Financial Account Information in Tax Matters, better known as the Common Reporting Standard to promote tax transparency and fight against tax evasion.
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="headingTwo">
                                                    <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        What is Common Reporting Standard (CRS)?
        </button>
      </h5>
                                                </div>
                                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="headingThree">
                                                    <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        Who developed Common Reporting Standard (CRS)?
        </button>
      </h5>
                                                </div>
                                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="about_heading text-center security_head">
                                        <h1>Code of Conduct – HCB’s voluntary codes of conduct and public commitments</h1>
                                    </div>
                                    <div class="panel-text">
                                        <p class="text-left">
                                            Habib Canadian Bank complies with many industry codes of Conduct and Public Commitments that are designed to protect consumers. The Financial Consumer Agency of Canada (FCAC) monitors our adherence to Codes of Conduct and Public Commitments.
                                        </p>
                                        <b class="pb-3 d-block">Bank’s voluntary codes of conduct:</b>
                                        <p class="text-left pt-10">
                                            Canadian Bankers Association’s Code of Conduct for Authorized Insurance Activities This code of conduct outlines the standards for selling credit, travel and personal accident insurance by branch staff members. It deals with training, disclosure, promotion practices, customer privacy protection and complaints procedures. NOTE: This particular code of conduct does not apply to the Bank, as the Bank does not sell any insurance products to its clientele. Canadian Code of Practice for Consumer Debit Card Services This code of conduct is designed to protect Canadian consumers who use debit card services. It outlines industry practices as well as the responsibilities of and consumers and the industry in relation to debit card transactions and liability. Code of Conduct for Credit and Debit Card Industry in Canada This code of conduct applies to debit and credit card networks and their participants. It outlines payment card networks operators’ responsibilities for providing information, flexibility and choice to merchants. Code of Conduct for Federally Regulated Financial Institutions—Mortgage Prepayment Information.</p>

                                        <p>Canadian Bankers Association’s Code of Conduct for Authorized Insurance Activities This code of conduct outlines the standards for selling credit, travel and personal accident insurance by branch staff members. It deals with training, disclosure, promotion practices, customer privacy protection and complaints procedures. NOTE: This particular code of conduct does not apply to the Bank, as the Bank does not sell any insurance products to its clientele. Canadian Code of Practice for Consumer Debit Card Services This code of conduct is designed to protect Canadian consumers who use debit card services. It outlines industry practices as well as the responsibilities of and consumers and the industry in relation to debit card transactions and liability. Code of Conduct for Credit and Debit Card Industry in Canada This code of conduct applies to debit and credit card networks and their participants. It outlines payment card networks operators’ responsibilities for providing information, flexibility and choice to merchants. Code of Conduct for Federally Regulated Financial Institutions—Mortgage Prepayment Information.</p>
                                    </div>

                                </div>

                            </div>

                            <div class="ready-start w-100">
                                <div class="container">
                                    <div class="row">
                                        <div class="ready_start_text">
                                            <h1>READY TO GET STARTED? </h1>
                                            <a href="javascript:void(0)" class="common_btn">Apply Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="about_heading text-center security_head">
                                    <h1>Privacy Notice</h1>
                                </div>
                                <div class="panel-text">
                                    <p class="text-center">
                                        At Habib Canadian Bank (HCB), we have always made it a priority to protect your personal information. HCB is committed to keeping customers’ information accurate, confidential, secure and private. HCB’s relationship with its customers is built on this commitment.
                                    </p>

                                </div>

                                <div class="question_answer">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="heading4">
                                                <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
        What is Common Reporting Standard (CRS)?
        </button>
      </h5>
                                            </div>

                                            <div id="collapse4" class="collapse show" aria-labelledby="heading4" data-parent="#accordion">
                                                <div class="card-body">
                                                    In October 2014, over 100 countries endorsed the Standard for Automatic Exchange of Financial Account Information in Tax Matters, better known as the Common Reporting Standard to promote tax transparency and fight against tax evasion.
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="heading5">
                                                    <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
        What is Common Reporting Standard (CRS)?
        </button>
      </h5>
                                                </div>
                                                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="heading6">
                                                    <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
        Who developed Common Reporting Standard (CRS)?
        </button>
      </h5>
                                                </div>
                                                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>


                                                               <div class="card">
                                                <div class="card-header" id="heading7">
                                                    <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
        What information we collect?
        </button>
      </h5>
                                                </div>
                                                <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            

            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>

    </html>
