<?php  $page_class = "student_fine_print"; $page_bread = "<span>Fine </span> Print" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1 class="flex-wrap"><?= $page_bread;?></h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 5</h3>
                                                    <p>THE FINE PRINT</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100"> 
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label_text">Your Tax Residency Details</label>
                                                    <p class="heading_text">
                                                        Tax residency is a method used by the Canada Revenue Agency (CRA) and by other countries to determine how much tax you should pay. It is your responsibility to determine your tax residency status in other countries.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you Tax Residant of Canada?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you US person for tax purpose?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">&nbsp;</label>
                                                    <input type="text" name="" class="form-control" placeholder="US Tax Identification Number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you US person for tax purpose?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Do you pay taxes in any countries?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <select class="form-control">
                                                            <option disabled selected>Country</option>
                                                            <option>Country</option>
                                                            <option>Country</option>
                                                            <option>Country</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">&nbsp;</label>
                                                    <input type="text" name="" class="form-control" placeholder="Tax Identification Number">
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <h1 class="form_heading w-100">The Fine Print</h1>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p>By selecting Send application I declare, as required by Canadian law, that the tax residency information and U.S. Person status provided (including any Tax Identification Number) are, to the best of my knowledge and belief, correct and complete.</p>
                                                    <p>If any of this information changes, I will provide Habib Canadian Bank with the updated information within 30 days.</p>
                                                    <p>Failure to provide satisfactory self-certification of Tax residency or U.S. Person status may result in my account information being reported to the relevant tax authority and I may be subject to a penalty under the Income Tax Act.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="student-account-product-selection" class="common_btn next_btn black_btn">BACK</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="javascript:void(0)" class="common_btn next_btn green">SUBMIT APPLICATION</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>