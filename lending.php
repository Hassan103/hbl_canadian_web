<?php  $page_class = "revised_saving_account"; $page_bread = "<span>Savings</span> Account" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page revised_saving_account">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;    ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page_heading_two_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page_heading_two">
                                    Personal
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="gurantee_text_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="revised-saving-account">
                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Guaranteed Investment Certificate – GIC)</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                The Longer You Save, The More You Earn. Invest with Habib Canadian Bank and enjoy the following benefits:
                                            </p>
                                            <ul class="default-list">
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Avail a loan secured against your deposit (corresponding to the term of your deposit )*</li>
                                                <li>Minimum Deposit of only $2,000 for Personal accounts</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current term deposit rates.</span></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Hi- Rate Savings Plus</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Short term investment with immediate or frequent access to your funds, Habib Canadian Bank’s Hi-Rate Savings Plus account enjoys the following benefits:
                                            </p>
                                            <ul class="default-list">
                                                <li>Immediate access to your funds</li>
                                                <li>Savings can easily be moved to a Term Deposit at your request</li>
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Avail Automatic Savings Plan at no charge</li>
                                                <li>Available for personal clients with a minimum balance of only $2000</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current Hi-Rate Savings Plus rates</span></div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Hi-Rate Deposit Notice Account (Call account)</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Habib Canadian Bank`s unique product – the Notice Deposit account offers high interest rates with the flexibility to withdraw the funds at your convenience. This account provides the following benefits:</p>
                                            <ul class="default-list">
                                                <li>Funds can be withdrawn by providing a notice of withdrawal at any time. The funds are made available to you on the day the notice period is over. </li>
                                                <li>Choice of 32-day or 45-day notice period product is available</li>
                                                <li>Minimum balance of $5,000</li>
                                                <li>Partial withdrawals are allowed as long as $5,000 remains in the account. Notice period must be satisfied before funds are paid out</li>
                                                <li>CDIC insured for eligible deposits</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current Hi-Rate Savings Plus rates</span></div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>RRSP Account</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Registered Retirement Savings Plan (RRSP) is a personal savings plan introduced by the Canadian federal government encouraging Canadian residents to save for their retirement on a tax-sheltered basis. With Habib Canadian Bank, your RRSP investments can be held in an RRSP Savings account or an RRSP Term Deposit (GIC) account. With our RRSP account you enjoy the following benefits:</p>
                                            <ul class="default-list">
                                                <li>Your contributions are tax deductible but withdrawals are not**</li>
                                                <li>Your principal and returns are not affected by the daily fluctuations of the stock markets</li>
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Avail Automatic Savings Plan at no charge</li>

                                            </ul>
                                            <div class="text-center"> <span>View our current interest rates</span></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>TFSA Saving Accounts</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Whether you are saving for a future home, a car, renovations, your children’s education, or saving funds for emergencies, a TFSA savings account helps you grow your portfolio faster. With Habib Canadian Bank, your TFSA investments are held in either a TFSA savings account or a TFSA term deposit (GIC) account. Our TFSA accounts provide you with the following benefits:</p>
                                            <ul class="default-list">
                                                <li>You do not pay any tax on your interest income and your withdrawal</li>
                                                <li>Your principal and returns are not affected by the daily fluctuations of the stock markets</li>
                                                <li>TFSA does not require you to have earned income to invest</li>
                                                <li>You can indefinitely carry forward your TFSA unused contribution limit**</li>
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Avail Automatic Savings Plan at no charge</li>
                                                <li>Minimum Deposit of $2,000.00</li>

                                            </ul>
                                            <div class="text-center"> <span>View our current interest rates</span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ready-start">
                    <div class="ready_start_text">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <h1>READY TO GET STARTED?</h1>
                                </div>
                                <div class="col-md-4 text-md-right">
                                    <a href="javascript:void(0)" class="common_btn btn_green">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page_heading_two_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page_heading_two">
                                    Business
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gurantee_text_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="revised-saving-account">
                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Guaranteed Investment Certificate – GIC</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                The Longer You Save, The More You Earn. Invest with Habib Canadian Bank and enjoy the following benefits:</p>
                                            <ul class="default-list">
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Secure a loan against your deposit (corresponding to the term of your deposit)*</li>
                                                <li>Minimum Deposit of $5,000 required for Business accounts</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current term deposit rates</span></div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Hi-Rate Savings Plus</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Short term investment with immediate or frequent access to your funds, Habib Canadian Bank’s Hi-Rate Savings Plus account enjoys the following benefits:</p>
                                            <ul class="default-list">
                                                <li>Savings can easily be moved to a GIC at your request</li>
                                                <li>CDIC insured for eligible deposits</li>
                                                <li>Make automatic contributions to your savings with Habib Canadian Automatic Savings Plan</li>
                                                <li>Available for business clients with a minimum balance of only $2000</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current Hi-Rate Savings Plus rates</span></div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="gurantee_head text-center">
                                            <h1>Hi-Rate Deposit Notice Account</h1>
                                        </div>

                                        <div class="gurantee_text">
                                            <p>
                                                Habib Canadian Bank`s unique product – the Notice Deposit account offers high interest rates with the flexibility to withdraw the funds at your convenience. This account provides the following benefits:</p>
                                            <ul class="default-list">
                                                <li>Funds can be withdrawn by providing a notice of withdrawal at any time. The funds are made available to you on the day the notice period is over. </li>
                                                <li>Choice of 32-day or 45-day notice period product is available</li>
                                                <li>Partial withdrawals are allowed as long as $5,000 remains in the account. Notice period must be satisfied before funds are paid out</li>
                                                <li>CDIC insured for eligible deposits</li>
                                            </ul>
                                            <div class="text-center"> <span>View our current Hi-Rate Deposit rates</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ready_open_account">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ready_account_text">
                                    <h1>     Ready to <br> open an account? </h1>
                                    <a href="javascript:void">CLICK HERE</a>

                                </div>
                            </div>

                            <div class="col-md-6" id="opointment_id">
                                <div class="ready_men_icon">
                                    <img src="assets/images/men-icon.png" alt="">
                                    <p >BOOK A MEETING
                                        <br> WITH EXPERT.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

            <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
            <!-- End Js Scripts -->
    </body>

    </html>
