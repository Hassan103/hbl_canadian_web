$('input[type="file"]').change(function (e) {
    var fileName = e.target.files[0].name;
    var get_parent = $(this).closest('.file_upload');
    get_parent.find('label').text(fileName);
});


// Steps Form
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).closest('fieldset');
    next_fs = $(this).closest('fieldset').next();
    console.log(current_fs, 'current_fs');
    console.log(next_fs, 'next_fs');
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'transform': 'scale(' + scale + ')' });
            next_fs.css({ 'left': left, 'opacity': opacity });
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeOutQuint'
    });
});

$(".previous").click(function () {
    if (animating) return false;
    animating = true;


    current_fs = $(this).closest('fieldset');
    next_fs = $(this).closest('fieldset').next();

    current_fs = $(this).closest('fieldset');
    previous_fs = $(this).closest('fieldset').prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeOutQuint'
    });
});

$(".submit").click(function () {
    return false;
})

// Home Slider

$('.single-item').slick({
    dots: true,
    prevArrow: false,
    nextArrow: false
});

//Custom Search Filter

function mySearchFilter() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue, button;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    card - body
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("button")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}




// Saving Page
$('.revised-saving-account').slick({
    dots: true,
    prevArrow: '<span class="c-arrow left_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M17,3h2c0.386,0,0.738,0.223,0.904,0.572s0.115,0.762-0.13,1.062L11.292,15l8.482,10.367 c0.245,0.299,0.295,0.712,0.13,1.062S19.386,27,19,27h-2c-0.3,0-0.584-0.135-0.774-0.367l-9-11c-0.301-0.369-0.301-0.898,0-1.267 l9-11C16.416,3.135,16.7,3,17,3z"></path></svg></span>',
    nextArrow: '<span class="c-arrow right_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M12,27h-2c-0.386,0-0.738-0.223-0.904-0.572s-0.115-0.762,0.13-1.062L17.708,15L9.226,4.633 c-0.245-0.299-0.295-0.712-0.13-1.062S9.614,3,10,3h2c0.3,0,0.584,0.135,0.774,0.367l9,11c0.301,0.369,0.301,0.898,0,1.267l-9,11 C12.584,26.865,12.3,27,12,27z"></path></svg></span>',
});

$('.personal-revised-saving-account').slick({
    dots: true,
    prevArrow: '<span class="c-arrow left_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M17,3h2c0.386,0,0.738,0.223,0.904,0.572s0.115,0.762-0.13,1.062L11.292,15l8.482,10.367 c0.245,0.299,0.295,0.712,0.13,1.062S19.386,27,19,27h-2c-0.3,0-0.584-0.135-0.774-0.367l-9-11c-0.301-0.369-0.301-0.898,0-1.267 l9-11C16.416,3.135,16.7,3,17,3z"></path></svg></span>',
    nextArrow: '<span class="c-arrow right_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M12,27h-2c-0.386,0-0.738-0.223-0.904-0.572s-0.115-0.762,0.13-1.062L17.708,15L9.226,4.633 c-0.245-0.299-0.295-0.712-0.13-1.062S9.614,3,10,3h2c0.3,0,0.584,0.135,0.774,0.367l9,11c0.301,0.369,0.301,0.898,0,1.267l-9,11 C12.584,26.865,12.3,27,12,27z"></path></svg></span>',
});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
var saving1 = getUrlParameter('saving1');
var saving2 = getUrlParameter('saving2');
var saving3 = getUrlParameter('saving3');
if (saving1) {
    $('.personal-revised-saving-account').slick('slickGoTo', 0);
}

if (saving2) {
    $('.personal-revised-saving-account').slick('slickGoTo', 1);
}
if (saving3) {
    $('.personal-revised-saving-account').slick('slickGoTo', 3);
}


// Timeline 
$('.time_line_slider').slick({
    dots: false,
    prevArrow: false,
    nextArrow: false,
    slidesToShow: 1,
    infinite: true,
    prevArrow: '<span class="c-arrow left_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M17,3h2c0.386,0,0.738,0.223,0.904,0.572s0.115,0.762-0.13,1.062L11.292,15l8.482,10.367 c0.245,0.299,0.295,0.712,0.13,1.062S19.386,27,19,27h-2c-0.3,0-0.584-0.135-0.774-0.367l-9-11c-0.301-0.369-0.301-0.898,0-1.267 l9-11C16.416,3.135,16.7,3,17,3z"></path></svg></span>',
    nextArrow: '<span class="c-arrow right_arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" fill="#fff"  width="20" height="20">    <path d="M12,27h-2c-0.386,0-0.738-0.223-0.904-0.572s-0.115-0.762,0.13-1.062L17.708,15L9.226,4.633 c-0.245-0.299-0.295-0.712-0.13-1.062S9.614,3,10,3h2c0.3,0,0.584,0.135,0.774,0.367l9,11c0.301,0.369,0.301,0.898,0,1.267l-9,11 C12.584,26.865,12.3,27,12,27z"></path></svg></span>',

});


// popup Js
$(document).ready(function () {
    $('.modal_one').removeClass('show');
    $('#call_pop').on('click', function () {
        $('.modal_one').removeClass('hide');
        $('.modal_one').addClass('active');
        $('.modal_one  + .bgOverly').css('display', 'block');
        $('body').css('overflow', 'hidden');
    });
    $('.close_btn, .bgOverly').on('click', function () {
        $('.modal_one').removeClass('active');
        $('.modal_one').addClass('hide');
        $('.bgOverly').css('display', 'none');
        $('body').css('overflow', 'auto');
    });
})



// popup Js  Two
$(document).ready(function () {


    $('#opointment_id').on('click', function () {
        $('.modal_two').removeClass('hide');
        $('.modal_two').addClass('active');
        $('.modal_two + .bgOverly').css('display', 'block');
        $('body').css('overflow', 'hidden');
    });
    $('.close_btn, .bgOverly').on('click', function () {
        $('.modal_two').removeClass('active');
        $('.modal_two').addClass('hide');
        $('.bgOverly').css('display', 'none');
        $('body').css('overflow', 'auto');
    });
})



// Time Line

$('.time_line_Pointer ul li').on('click', function () {

    let get_tab = $(this).attr('data-tab');
    $('.time_line_Pointer ul li').removeClass('active');
    $(this).addClass('active');
    console.log(get_tab);
    $('.commont_tab').removeClass('active');
    $('#tab-' + get_tab).addClass('active');
});



$(document).ready(function () {

    /*
    
        How to use:
        1)  Copy this jQuery to your project
        2)  Add [data-search] to search input
        3)  Add [data-filter-item] to the items that should be filtered
        4)  Add [data-filter-name="SEARCH TERM"] to the filter-items
    
    */

    $('[data-search]').on('keyup', function () {
        console.log("Testimg...")
        var searchVal = $(this).val();
        var filterItems = $('[data-filter-item]');

        if (searchVal != '') {
            filterItems.addClass('hidden');
            $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('hidden');
        } else {
            filterItems.removeClass('hidden');
        }
    });



    // $('#search_question').on('click', function(event){
    //     event.preventDefault();
    //     var get_id = $('#search_feild').val().toLowerCase();
    //     var get_text =  $('#accordion ul li button');
    //       var get_all = [];
    //     get_text.map((index, value)  => {
    //         get_all.push(value); 
    //         if(get_all[index].innerHTML.toLowerCase() == get_id ){
    //             console.log("Match")
    //         }else{
    //             console.log("Not Match")
    //         }
    //     });



    // });
});






// var fu1 = document.getElementById("file");
//             fu1.addEventListener('change', function(){

//                  var filename = this.value;
//                  var avc = filename.substr(filename.lastIndexOf("'\'") + 1);
//                  console.log(avc)
//             })


var rangeSlider = function () {
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');

    slider.each(function () {

        value.each(function () {
            var value = $(this).prev().attr('value');
            $(this).html(value);
        });

        range.on('input', function () {
            $(this).next(value).html(this.value);
        });
    });
};

rangeSlider();