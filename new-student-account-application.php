<?php  $page_class = "new_student_account"; $page_bread = "<span>NEW ACCOUNT</span> Student Direct Steam"; ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;?>
                                        <p>To be eligible to apply for a HCB Student Direct Stream Program, you must:</p>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                   <div class="row">
                                       <div class="col-md-8">
                                            <div class="steps d-flex"> 
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 1</h3>
                                                    <p>CLIENT INFORMATION</p>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="col-md-4">
                                           <div class="all_steps d-flex align-items-center">
                                               <ul class="default-list w-100">
                                                   <li class="active"></li>
                                                   <li></li>
                                                   <li></li>
                                                   <li></li>
                                                   <li></li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                </div>
                            <div class="new_account_forms">
                                <form action="" class="commonForm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="student_eligbility mb-5">
                                                <h2>To be eligible to apply for a HCB Student Direct Stream Program, you must:</h2>
                                                <div class="d-flex flex-wrap mt-5">
                                                    <div class="col-md-3">
                                                        <div class="std_options_img text-center">
                                                               
                                                             <svg width="100"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" fill="#444b54"><path d="M25 1.05c-.218 0-.434.07-.62.21l-23 17.95c-.43.34-.51.97-.17 1.41.34.43.97.51 1.41.17L4 19.71V46c0 .55.45 1 1 1h14V29h12v18h14c.55 0 1-.45 1-1V19.71l1.38 1.08c.19.14.4.21.62.21.3 0 .59-.13.79-.38a1.01 1.01 0 00-.17-1.41l-23-17.95a1.022 1.022 0 00-.62-.21zM35 5v1.05l6 4.68V5h-6z"/></svg>
                                                            <p>
                                                               Be an individual who is planning on studying in Canada
                                                            </p>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="std_options_img text-center">
                                                               
                                              <svg xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 30 30" fill="#444b54"><path d="M15 3a6 6 0 00-6 6v1a6 6 0 1012 0V9a6 6 0 00-6-6zm-.002 16c-4.006 0-9.146 2.167-10.625 4.09C3.459 24.279 4.329 26 5.828 26H24.17c1.499 0 2.369-1.721 1.455-2.91C24.146 21.168 19.004 19 14.998 19z"/></svg>
                                                            <p>
                                                             Must be a minimum of 13 years of age to apply
                                                            </p>

                                                        </div>
                                                    </div>
                                                              <div class="col-md-3">
                                                        <div class="std_options_img text-center">
                                                               
                                                           <svg xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 30 30" fill="#444b54"><path d="M15 3a6 6 0 00-6 6v1a6 6 0 1012 0V9a6 6 0 00-6-6zm-.002 16c-4.006 0-9.146 2.167-10.625 4.09C3.459 24.279 4.329 26 5.828 26H24.17c1.499 0 2.369-1.721 1.455-2.91C24.146 21.168 19.004 19 14.998 19z"/></svg>
                                                            <p>
                                                                Not be coming to Canada on a tourist/visitor visa
                                                            </p>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="std_options_img text-center">
                                                               
                                                              <svg width="100" fill="#444b54" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16 4c-2.2 0-4 1.8-4 4 0 1.035.41 1.973 1.063 2.688-.407.199-.766.445-1.126.718C11.649 9.484 9.998 8 8 8c-2.2 0-4 1.8-4 4 0 1.066.434 2.031 1.125 2.75-.258.14-.516.293-.75.469A6.001 6.001 0 002 20h2c0-1.309.629-2.457 1.594-3.188A3.96 3.96 0 018 16c.621 0 1.203.168 1.75.438l1.094.53.312-1.187C11.703 13.617 13.652 12 16 12a4.984 4.984 0 014.844 3.75l.312 1.219 1.094-.563A3.92 3.92 0 0124 16c2.219 0 4 1.781 4 4h2c0-2.254-1.273-4.227-3.125-5.25A3.958 3.958 0 0028 12c0-2.2-1.8-4-4-4-1.996 0-3.648 1.484-3.938 3.406a6.784 6.784 0 00-1.125-.719A3.978 3.978 0 0020 8c0-2.2-1.8-4-4-4zm0 2c1.117 0 2 .883 2 2s-.883 2-2 2-2-.883-2-2 .883-2 2-2zm-8 4c1.117 0 2 .883 2 2s-.883 2-2 2-2-.883-2-2 .883-2 2-2zm16 0c1.117 0 2 .883 2 2s-.883 2-2 2-2-.883-2-2 .883-2 2-2zm-8 6c-2.2 0-4 1.8-4 4 0 1.066.434 2.031 1.125 2.75C11.273 23.773 10 25.746 10 28h2c0-2.219 1.781-4 4-4 2.219 0 4 1.781 4 4h2c0-2.254-1.273-4.227-3.125-5.25A3.958 3.958 0 0020 20c0-2.2-1.8-4-4-4zm0 2c1.117 0 2 .883 2 2s-.883 2-2 2-2-.883-2-2 .883-2 2-2z"/></svg>
                                                            <p>
                                                               Not be opening an account on behalf of a third party
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
      
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Middle Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="date" class="form-control" placeholder="Date of Birth (dd/mm/yyyy)">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                              <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Confirm Email">
                                            </div>
                                        </div> 
                                    </div>

                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Phone Number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                              <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Alternate Phone Number">
                                            </div>
                                        </div> 
                                    </div>
                         

                               <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                               <input type="text" class="form-control" placeholder="Address Line 1">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            
                                        </div> 
                                    </div>




                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        
                                                <input type="text" class="form-control" placeholder="Address Line 2">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                <input type="text" class="form-control" placeholder="City">
                                            </div>
                                        </div> 
                                    </div>
                         

                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="d-flex">
                                                <div class="form-group col-md-4 pl-0"> 
                                                    <input type="text" class="form-control" placeholder="State ">
                                                </div>
                                                <div class="form-group col-md-8 p-0"> 
                                                    <input type="text" class="form-control" placeholder="Zip Code">
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                 <select name="" id="" class="form-control">
                                                     <option value="">Country</option>
                                                     <option value="">Country</option>
                                                     <option value="">Country</option>
                                                     <option value="">Country</option> 
                                                 </select>
                                            </div>
                                        </div> 
                                    </div>


                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        
                                                <input type="text" class="form-control" placeholder="Parents / Guardian’s Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                <input type="text" class="form-control" placeholder="Parents / Guardian’s Name">
                                            </div>
                                        </div> 
                                    </div>


                                    <div class="row">
                                     
                                        <div class="col-md-6">
                                            <div class="form-group agree"> 
                                                <p> <span> <input type="radio" name="" class="customRadio"> </span> I agree to be contacted by Habib Canadian Bank in the 
                                                    future and agree with the Terms & Conditions including the Anti-spam and Privacy Policy.</p> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            <div class="form-group"> 
                                               <a href="student-account-personal-info.php" class="common_btn next_btn green">NEXT STEP</a>
                                            </div>  
                                        </div>
                                       
                                    </div>

                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body> 
    </html>