<?php  $page_class = "travel_canada"; $page_bread = "<span>Travelling to</span> Canada"; ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;?>
                                        
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                   <div class="row">
                                       <div class="col-md-8">
                                            <div class="steps d-flex"> 
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 3</h3>
                                                    <p>TRAVELLING TO CANADA</p>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="col-md-4">
                                           <div class="all_steps d-flex align-items-center">
                                               <ul class="default-list w-100">
                                           
                                                   <li class="active"></li>
                                                   <li class="active"></li>
                                                   <li class="active"></li>
                                                   <li></li>
                                                   <li></li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                </div>
                            <div class="new_account_forms">
                                <form action="" class="commonForm">
                              
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text_label">
                                                    Passport Details
                                                </label>
                                                <input type="text" class="form-control" placeholder="Passport Number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text_label">
                                                    &nbsp;
                                                </label>
                                                <select class="form-control">
                                                    <option disabled selected>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Passport Issue Date">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="date" class="form-control" placeholder="Passport Expiry Date">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="file_upload">
                                         <label for="file">
                                            <input type="file" name="" id="file" accept="images/*">
                                            Upload Passport Copy
                                        </label>
                                         </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            
                                        </div> 
                                    </div>
                         

                               <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Education Institute">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="file_upload">
                                                    <label for="file">
                                                        <input type="file" name="" id="file" accept="images/*">
                                                        Upload Institute Offer Letter
                                                    </label>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                              <div class="form-group">
                                       
                                                <select class="form-control">
                                                    <option disabled selected>Secondry ID Type</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="file_upload">
                                                    <label for="file">
                                                        <input type="file" name="" id="file" accept="images/*">
                                                        Upload Institute Offer Letter
                                                    </label>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                   <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="student-account-personal-info.php" class="common_btn next_btn black_btn">BACK</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="student-account-product-selection.php" class="common_btn next_btn green">NEXT STEP</a>
                                            </div>
                                        </div>
                                    </div> 
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body> 
    </html>