<?php  $page_class = "student_personal_info"; $page_bread = "<span>Personal </span> Information" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1 class="flex-wrap"><?= $page_bread;?></h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 2</h3>
                                                    <p>PERSONAL INFORMATION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                             
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">
                                          
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                   Is your home address the same as your mailing address? 
                                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                            </label>

                                                        </div>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                    &nbsp;<span class="steric"></span>
                                                <div class="form-group">
                                                    <input type="text" name="Address Line 1" 
                                                    placeholder="Address Line 1" 
                                                    class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="Address Line 1" 
                                                    placeholder="Address Line 2" 
                                                    class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option selected disable>City</option>
                                                        <option selected disable>City</option>
                                                        <option selected disable>City</option>
                                                        <option selected disable>City</option>
                                                        <option selected disable>City</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                             <select class="form-control">
                                                                 <option disabled selected>State</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                             </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                           <input type="text" name="" class="form-control" placeholder="Zip Code">
                                                        </div>
                                                </div>    
                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                      <select class="form-control">
                                                                 <option disabled selected>Country</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                                 <option>State</option>
                                                             </select>
                                                </div>
                                            </div>
                                        </div>
<hr class="line_break">
                                     
                                          
                                       
  
                              
                                
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <select class="form-control">
                                                         <option disabled selected>Level Of Education</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="School Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <select class="form-control">
                                                         <option disabled selected>Employement Status</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                               <div class="form-group">
                                                   <select class="form-control">
                                                         <option disabled selected>Industry</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                         <option>State</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>      
                                              <div class="row">
                                            <div class="col-md-6">
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Occupation">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Previous Employer">
                                                </div>
                                            </div>
                                        </div>
                                
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="new-student-account-application.php" class="common_btn next_btn black_btn">BACK</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="student-travel-canada.php" class="common_btn next_btn green">NEXT STEP</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>