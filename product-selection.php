<?php  $page_class = "product_selection"; $page_bread = "<span>Product </span> Selection" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1 class="flex-wrap"><?= $page_bread;?></h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 3</h3>
                                                    <p>PRODUCT SELECTION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select the type of account you wish to open</label>
                                                    <select class="form-control">
                                                        <option value="" disabled selected>Select An Account*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                    <label class="label_text">Please select an intended use for Chequing Account</label>
                                                    <select class="form-control">
                                                        <option value="" disabled selected>Select one</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">&nbsp;</label>
                                                 <p class="m-0">Please enter a value greater than or equal to 1000.</p>
                                                </div>

                                            </div>
                                        </div>

                                        <hr class="line_break">
                                   
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>

                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Would you like to open Additional Accounts?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label> 

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label_text">Addional Account types?</label>
                                                    <div class="d-flex flex-wrap">
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked">
                                                                <label for='checked'>
                                                                    <p class="checkbox_text pl-2">Non-Registered GIC</p>
                                                                </label>

                                                            </div>
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked4">
                                                                <label for='checked4'>
                                                                    <p class="checkbox_text pl-2">RRSP</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked2">
                                                                <label for='checked2'>
                                                                    <p class="checkbox_text pl-2">TFSA </p>
                                                                </label>

                                                            </div>
                                                                      <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked5">
                                                                <label for='checked5'>
                                                                    <p class="checkbox_text pl-2">Locked-in RRSP (LIRA)</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked3">
                                                                <label for='checked3'>
                                                                    <p class="checkbox_text pl-2">Spousal RRSP</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                           

                                  

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control" class="" placeholder="Beneficiary Name">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control" placeholder="Beneficiary Relation">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                         <div class="form-group">
                                            
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Intended use</label>
                                                    <input type="text" name="" class="form-control" placeholder="Intended use">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="form-group m-0">
                                                    <p class="m-0">Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                  
                                        </div>
                                 
                                        <div class="row">
                                            <div class="col-md-10 mx-auto">
                                                <div class="show_cheque text-center">
                                                    <img src="assets/images/check.png">
                                                    <p>To complete the process of opening your account, please make a cheque payable to yourself with an amount and mail the cheque to the following address:</p>
                                                    <div class="hbl_checkinfo text-center">
                                                        <h5>Habib Canadian Bank</h5>
                                                            <p>ATTN: Account Opening</p>
                                                            <p>918 Dundas Street East, Suite 400</p>
                                                            <p>Mississauga, Ontario, L4Y 4H9</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <button class="common_btn next_btn black_btn">BACK</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <button class="common_btn next_btn green">NEXT STEP</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>