<?php  $page_class = "student-gic-program"; $page_bread = "<span></span>" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page student_gic_page">
                <div class="heaader_inner student_gic_bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;    ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="gic_texual_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="gic_student_head h-100 d-flex align-items-center">
                                    <h1>
                    Student <br> <span>GIC</span> Program
                    </h1>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="gic_student_text">
                                    <p>
                                        Studying in Canada presents many opportunities for international students, with access to some of the best universities and qualifications that are recognized and respected the world over. Habib Canadian Bank offers every student an opportunity to plan their study in Canada, with the ease of our financial solutions.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ready-start">
                    <div class="ready_start_text">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <h5>
                                    Are you a student from Pakistan, South Africa, UAE or Hong Kong?
                                    </h5>
                                </div>
                                <div class="col-lg-8 text-lg-right">
                                    <a href="javascript:void(0)" class="common_btn">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="how_gic_work">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="how_gic_work_head">
                                    <h1>
                    How the GIC program work?</h1>
                                </div>

                                <div class="gic_overview">
                                    <h4>
                        Overview
                    </h4>
                                    <p>A GIC is an investment accoutn that offers a guaranteed interest rate over a fixed period of time. With our Student GIC Program you can invest between $10,000 CAD - $50,000 CAD. </p>
                                </div>
                            </div>
                        </div>
                        <div class="gic_steps">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="gic_steps_box">
                                        <h3>Step 1</h3>
                                        <p>A wire transfer of the total amount plus fee should be initiated before arrival</p>
                                    </div>

                                    <div class="gic_steps_box">
                                        <h3>Step 3</h3>
                                        <p>Receive a portion of your investment back each month for 12 months</p>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="gic_steps_box">
                                        <h3>Step 2</h3>
                                        <p>$2,000 CAD plus any interest on arrival, deposited into your student bank account</p>
                                    </div>

                                    <div class="gic_steps_box">
                                        <h3>Step 4</h3>
                                        <p>After 12 months, you’ll have received your entire investment back, plus interest</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="whats_next text-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>What's Next ?</h1>
                                <div class="whats_next_img">
                                    <img src="assets/images/arrow-bottom.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>
    </html>
