<?php  $page_class = "our_team"; $page_bread = "<span>BE A PART OF</span> OUR TEAM" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>HHabib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page our_team_page">
                <div class="heaader_inner our-team-banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;    ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
            <section class="our_team_form_wrapper">
                <div class="container">
                    <div class="row our-team-wrap">
                        <div class="col-md-6 p-0">
                            <div class="our_team_form">
                                <div class="linkden-box">
                                    <img src="assets/images/linkden-icon.png">
                                            <div>
                                                <a href="javascript:void(0)" class="btn_green btn_grey common_btn w-100 text-center mt-3 d-block">Apply Now</a>
                                            </div>
                                </div>           
                            </div>
                        </div>
                        <div class="col-md-6 p-0">
                            <div class="our_team_text">
                                <h5>We are always on the lookout for talented people.</h5>
                                <p>   You can become a part of our team if you are ambitious, passionate and can support our mission to develop and deliver solutions that help our clients simplify, manage and maximize their banking needs. You will be rewarded with an opportunity to learn and grow in an environment based on HCB’s values. </p>

 <p> HCB’s recruitment policy and processes ensure that candidates are provided with equal opportunities to secure a position within the bank, provided they meet the bank’s stated criteria on candidate selection.</p>


                            </div> 
                        </div>
                    </div>
                </div>
            </section>

            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->
                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->

             
    </body>

    </html>