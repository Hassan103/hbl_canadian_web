<?php  $page_class = "home_page"; ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
 
      <title>Personal, Business Banking & Investments | Habib Canadian Bank</title>
      <?php include('includes/css.php'); ?>
   </head>
   <body>
      <!-- Header -->
      <?php include('includes/header.php'); ?>
      <!-- End Navigation Bar -->
      <!-- Slider -->
      <div class="single-item">
         <div class="item">
            <div class="MainSlider_Wrapper" style="background-image:url(assets/images/slider-01.png)">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="MainSliderText">
                           <h1>House Mortage <br /> Starting From 3.99%</h1>
                           <a href="javascript:void(0)" class="arrow_btn">
                              Learn More 
                              <span>
                                 <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                                    <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                                 </svg>
                              </span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="slider_sign_wrapper">
                           <div class="slider_sign">
                              <h2>Sign in to <br> your online <br> banking account</h2>
                              <a href="javascript:void(0)" class="common_btn btn_green">Click Here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="MainSlider_Wrapper" style="background-image:url(assets/images/slider-02.png)">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="MainSliderText">
                           <h1>STUDENTS GIC
<br /> PROGRAM</h1>
                           <a href="student-gic-program.php" class="arrow_btn">
                              Learn More 
                              <span>
                                 <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                                    <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                                 </svg>
                              </span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="slider_sign_wrapper">
                           <div class="slider_sign">
                              <h2>Sign in to <br> your online <br> banking account</h2>
                              <a href="javascript:void(0)" class="common_btn grey_btn">Click Here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="MainSlider_Wrapper" style="background-image:url(assets/images/slider-03.jpg)">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="MainSliderText">
                           <h1>House Mortage <br /> Starting From 3.99%</h1>
                           <a href="javascript:void(0)" class="arrow_btn">
                              Learn More 
                              <span>
                                 <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                                    <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                                 </svg>
                              </span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="slider_sign_wrapper">
                           <div class="slider_sign">
                              <h2>Sign in to <br> your online <br> banking account</h2>
                              <a href="javascript:void(0)" class="common_btn grey_btn">Click Here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="MainSlider_Wrapper" style="background-image:url(assets/images/slider-04.jpg)">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="MainSliderText">
                           <h1>House Mortage <br /> Starting From 3.99%</h1>
                           <a href="javascript:void(0)" class="arrow_btn">
                              Learn More 
                              <span>
                                 <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                                    <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                                 </svg>
                              </span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="slider_sign_wrapper">
                           <div class="slider_sign">
                              <h2>Sign in to <br> your online <br> banking account</h2>
                              <a href="javascript:void(0)" class="common_btn grey_btn">Click Here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="item">
            <div class="MainSlider_Wrapper" style="background-image:url(assets/images/slider-05.png)">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="MainSliderText">
                           <h1>House Mortage <br /> Starting From 3.99%</h1>
                           <a href="javascript:void(0)" class="arrow_btn">
                              Learn More 
                              <span>
                                 <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                                    <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                                 </svg>
                              </span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="slider_sign_wrapper">
                           <div class="slider_sign">
                              <h2>Sign in to <br> your online <br> banking account</h2>
                              <a href="javascript:void(0)" class="common_btn grey_btn">Click Here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End Slider -->
      <!-- Header -->
      <!-- Packages -->
      <section class="packges_wrapper">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="packges_box saving_box">
                     <h1>Savings</h1>
                     <p>Turn your goals into reality. It’s easier to reach your savings goals when you have the right account.
                     </p><br>
                     <a href="revised-saving-account.php" class="common_btn btn_green-light-x">Learn more</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="packges_box lending_box">
                     <h1>Lending</h1>
                     <p>Continue your journey without any speed bumps. Realize your dreams with the freedom and flexiblity of our mortgage solutions.
                     </p>
                     <a href="lending.php" class="common_btn btn_green-light">Learn More</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="packges_box trade_finance">
                     <h1>Trade Finance</h1>
                     <p>Trade with ease and confidence. Our credit and trade finance services will help you grow business outside of Canada
                     </p><br>
                     <a href="trade-finance.php" class="common_btn btn_green-light">Learn More</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End Packages -->
      <!-- Statistic -->
      <section class="statistcis_wrapper">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="statistic_box">
                     <h4>Grow Your Savings</h4>
                     <div class="states_percent">
                        1.30<span>%</span>
                     </div>
                     <a href="revised-saving-account.php?saving1#savings" class="arrow_btn">
                        Learn More 
                        <span>
                           <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                              <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                           </svg>
                        </span>
                     </a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="statistic_box">
                     <h4>Invest in GIC</h4>
                     <div class="states_percent">
                        1.75<span>%</span>
                     </div>
                     <a href="revised-saving-account.php?saving2#savings" class="arrow_btn">
                        Learn More 
                        <span>
                           <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                              <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                           </svg>
                        </span>
                     </a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="statistic_box">
                     <h4>Save with RRSP & TFSA</h4>
                     <div class="states_percent">
                        1.90<span>%</span>
                     </div>
                     <a href="revised-saving-account.php?saving3#savings" class="arrow_btn">
                        Learn More 
                        <span>
                           <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="1em" height="1em" style="-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg)" viewBox="0 0 32 32" transform="rotate(360)">
                              <path d="M12.97 4.28l-1.44 1.44L21.814 16 11.53 26.28l1.44 1.44 11-11 .686-.72-.687-.72-11-11z" fill="#000"/>
                           </svg>
                        </span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End Statistic -->
      <!-- Explore More Section -->
      <section class="explore_more_wrapper">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="section_head text-center">
                     <h1>Explore More</h1>
                  </div>
               </div>
            </div>
            <div class="explore_boxes">
               <div class="row">
                  <div class="col-md-4">
                     <div class="explore_more_box text-center">
                        <div class="explore_more_box_icon">
                           <img src="assets/images/01.png" alt="">
                        </div>
                        <div class="explore_more_box_text">
                           About Us
                           <br> & HBZ
                        </div>
                        <div class="explore_more_box_button">
                           <a href="about-us.php" class="border_btn btn_green">Learn More</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="explore_more_box text-center">
                        <div class="explore_more_box_icon">
                           <img src="assets/images/solutions.png" alt="">
                        </div>
                        <div class="explore_more_box_text">
                           Corporate Social
                           <br> Responsibility
                        </div>
                        <div class="explore_more_box_button">
                           <a href="javascript:void(0)" class="border_btn btn_green">Find More</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="explore_more_box text-center">
                        <div class="explore_more_box_icon">
                           <img src="assets/images/3.png" alt="">
                        </div>
                        <div class="explore_more_box_text">
                           Get
                           <br> In Touch
                        </div>
                        <div class="explore_more_box_button">
                           <a href="contact-us.php" class="border_btn btn_green">Locate a branch</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End Explore More Section -->
      
      <!-- Footer -->
      <?php  include('includes/footer.php'); ?>       
      <!-- End Footer -->


     <!-- Js Scripts -->
         <?php  include('includes/scripts.php'); ?>
      <!-- End Js Scripts -->
   </body>
</html>
