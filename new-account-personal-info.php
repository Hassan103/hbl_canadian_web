<?php  $page_class = "personal_info"; $page_bread = "<span>Personal </span> Information" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1 class="flex-wrap"><?= $page_bread;?></h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 2</h3>
                                                    <p>PERSONAL INFORMATION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                    
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">
                                            <h1 class="form_heading w-100 px-3">Personal Information</h1>
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                Will this account be used by or on behalf of a third party?<span class="steric">*</span>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                  To the best of your knowledge, are you a Politically Exposed Person or related to or a close associate of a Politically Exposed person?<span class="steric">*</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>
                                                <p class="hint">Habib Canadian Bank is unable to facilitate an online application by or on behalf of a third party. Please visit or call a branch to open an account.</p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>
                                                <p class="hint">Habib Canadian Bank is unable to facilitate an online application by or on behalf of a third party. Please visit or call a branch to open an account.</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Gender">
                                                        <option value="" disabled selected>Gender*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="date" class="form-control" placeholder="Date of Birthday">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Primary ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Secondry ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="form_heading w-100 px-3">Driving License</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control black" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Driving Lincense Number*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control black" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Issue Province*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue*">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Expiry*">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Secondry ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <h1 class="form_heading w-100 px-3">Permanent Resident Details</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                               <div class="form-group">
                                                   <input type="text" name="" class="form-control black" placeholder="Permanent Resident Address *">
                                               </div> 
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue *">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue *">
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">
                                        <h1 class="form_heading w-100">Mailing Address Information</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                    Have you been at your current address less then 2 Year?<span class="steric">*</span>
                                                </p>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                            </label>

                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Address Line 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <input type="text" class="form-control" placeholder="Address Line 2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="City">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="d-flex">
                                                    <div class="form-group col-md-4 pl-0">
                                                        <select name="" id="" class="form-control">
                                                            <option value="" disabled selected>State</option>
                                                            <option value="">Country</option>
                                                            <option value="">Country</option>
                                                            <option value="">Country</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-8 p-0">
                                                        <input type="text" class="form-control" placeholder="Zip Code">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select name="" id="" class="form-control">
                                                        <option value="" disabled selected>Country</option>
                                                        <option value="">Country</option>
                                                        <option value="">Country</option>
                                                        <option value="">Country</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="heading_text">
                                                        Is your home address the same as your mailing address?
                                                    </p>
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="line_break">
                                        <h1 class="form_heading w-100">Current Employee Information</h1>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">





                                                    <p>To help us better identify you please provide as detailed a job description or title as possible. If your job title is generic also specify in what industry or type of business you are employed. </p>

                                                    <p>For example, if you are a consultant you may state "Computer Networking Consultant" or "Communications and Public Relations Consultant." If you are self-employed, please specify the type of business you operate. If you are the President of a towing company use "President of Towing Company" or "Towing Operator" as your Detailed Occupation.</p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Employed">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Industry">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Accuaption">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Work Phone Number (Optional)">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="new-account-application.php" class="common_btn next_btn black_btn">BACK</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="product-selection.php" class="common_btn next_btn green">NEXT STEP</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>