<?php  $page_class = "new_account_creation"; $page_bread = "<span>NEW ACCOUNT</span> APPLICATION" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                            <fieldset>
                                <div class="new_account">
                                   <div class="row">
                                       <div class="col-md-8">
                                            <div class="steps d-flex"> 
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 1</h3>
                                                    <p>CLIENT INFORMATION</p>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="col-md-4">
                                           <div class="all_steps d-flex align-items-center">
                                               <ul class="default-list w-100">
                                                   <li class="active"></li>
                                                   <li></li>
                                                   <li></li>
                                                   <li></li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                </div>
                            <div class="new_account_forms">
                                <form action="" class="commonForm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Middle Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Social Insurance Number ">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            
                                        </div> 
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <p> We may need to use your Social Insurance Number to
                                                match credit bureau information and/or to protect you and
                                                Habib Canadian Bank from reporting errors and fraud. We also need to 
                                                have the number due to national and international regulations
                                                related to income tax reporting.
                                                </p>
                                            </div>
                                        </div>
                                    <div class="col-md-6">

                                    </div> 
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        
                                                <input type="text" class="form-control" placeholder="Address Line 1">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                <input type="text" class="form-control" placeholder="Phone Number">
                                            </div>
                                        </div> 
                                    </div>


                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        
                                                <input type="text" class="form-control" placeholder="Address Line 2">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                <input type="text" class="form-control" placeholder="City">
                                            </div>
                                        </div> 
                                    </div>
                         

                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="d-flex">
                                                <div class="form-group col-md-4 pl-0"> 
                                                    <input type="text" class="form-control" placeholder="State ">
                                                </div>
                                                <div class="form-group col-md-8 p-0"> 
                                                    <input type="text" class="form-control" placeholder="Zip Code">
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                 <select name="" id="" class="form-control">
                                                     <option value="">Country</option>
                                                     <option value="">Country</option>
                                                     <option value="">Country</option>
                                                     <option value="">Country</option> 
                                                 </select>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group"> 
                                                <input type="text" class="form-control" placeholder="Alternate Phone Number">
                                            </div>  
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                                <p> <span> <input type="radio" class="customRadio" name=""> </span> I agree to be contacted by Habib Canadian Bank in the 
                                                    future and agree with the Terms & Conditions including the Anti-spam and Privacy Policy.</p> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            <div class="form-group"> 
                                               <!-- <a href="new-account-personal-info.php" class="common_btn next_btn green">NEXT STEP</a> -->
                                               <input type="button" class="common_btn next_btn green next action-button" name="next"  value="NEXT STEP">
                                            </div>  
                                        </div>
                                       
                                    </div>

                                </form>
                            </div>
                            </fieldset>

                <fieldset>
                      
                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 2</h3>
                                                    <p>PERSONAL INFORMATION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                    
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">
                                            <h1 class="form_heading w-100 px-3">Personal Information</h1>
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                Will this account be used by or on behalf of a third party?<span class="steric">*</span>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                  To the best of your knowledge, are you a Politically Exposed Person or related to or a close associate of a Politically Exposed person?<span class="steric">*</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>
                                                <p class="hint">Habib Canadian Bank is unable to facilitate an online application by or on behalf of a third party. Please visit or call a branch to open an account.</p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>
                                                <p class="hint">Habib Canadian Bank is unable to facilitate an online application by or on behalf of a third party. Please visit or call a branch to open an account.</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Gender">
                                                        <option value="" disabled selected>Gender*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="date" class="form-control" placeholder="Date of Birthday">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Primary ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Secondry ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="form_heading w-100 px-3">Driving License</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control black" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Driving Lincense Number*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control black" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Issue Province*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue*">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Expiry*">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" placeholder="Primary ID*">
                                                        <option value="" disabled selected>Secondry ID*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <h1 class="form_heading w-100 px-3">Permanent Resident Details</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                               <div class="form-group">
                                                   <input type="text" name="" class="form-control black" placeholder="Permanent Resident Address *">
                                               </div> 
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue *">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" name="" class="form-control black" placeholder="Date of Issue *">
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">
                                        <h1 class="form_heading w-100">Mailing Address Information</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="heading_text">
                                                    Have you been at your current address less then 2 Year?<span class="steric">*</span>
                                                </p>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                            </label>

                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Address Line 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <input type="text" class="form-control" placeholder="Address Line 2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="City">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="d-flex">
                                                    <div class="form-group col-md-4 pl-0">
                                                        <select name="" id="" class="form-control">
                                                            <option value="" disabled selected>State</option>
                                                            <option value="">Country</option>
                                                            <option value="">Country</option>
                                                            <option value="">Country</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-8 p-0">
                                                        <input type="text" class="form-control" placeholder="Zip Code">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select name="" id="" class="form-control">
                                                        <option value="" disabled selected>Country</option>
                                                        <option value="">Country</option>
                                                        <option value="">Country</option>
                                                        <option value="">Country</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="heading_text">
                                                        Is your home address the same as your mailing address?
                                                    </p>
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="line_break">
                                        <h1 class="form_heading w-100">Current Employee Information</h1>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">





                                                    <p>To help us better identify you please provide as detailed a job description or title as possible. If your job title is generic also specify in what industry or type of business you are employed. </p>

                                                    <p>For example, if you are a consultant you may state "Computer Networking Consultant" or "Communications and Public Relations Consultant." If you are self-employed, please specify the type of business you operate. If you are the President of a towing company use "President of Towing Company" or "Towing Operator" as your Detailed Occupation.</p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Employed">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Industry">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Accuaption">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Work Phone Number (Optional)">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="new-account-application.php" class="common_btn next_btn black_btn">BACK</a> -->
                                                    <input type="button" class="common_btn next_btn black_btn previous" value="BACK">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="product-selection.php" class="common_btn next_btn green">NEXT STEP</a> -->
                                                    <input type="button" class="common_btn next_btn black_btn next" value="NEXT STEP">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                </fieldset>

<fieldset>
 
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 3</h3>
                                                    <p>PRODUCT SELECTION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                    
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                           
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select the type of account you wish to open</label>
                                                    <select class="form-control">
                                                        <option value="" disabled selected>Select An Account*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Account Holder Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select GIC Options</label>
                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select GIC Options*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Financial Institution?</label>

                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>

                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Would you like to open Additional Accounts?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label_text">Addional Account types?</label>
                                                    <div class="d-flex flex-wrap">
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked">
                                                                <label for='checked'>
                                                                    <p class="checkbox_text pl-2">Chequing Account</p>
                                                                </label>

                                                            </div>
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked4">
                                                                <label for='checked4'>
                                                                    <p class="checkbox_text pl-2">Chequing Account</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked2">
                                                                <label for='checked2'>
                                                                    <p class="checkbox_text pl-2">Chequing Account</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked3">
                                                                <label for='checked3'>
                                                                    <p class="checkbox_text pl-2">Chequing Account</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">GIC Options</label>
                                                    <input type="text" class="form-control" placeholder="Beneficiary Name">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Financial Institutions to transfer from </label>

                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select Financial Institutions</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>

                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Other</label>
                                                    <input type="text" name="" class="form-control" class="Other Institutions">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <input type="text" name="" class="form-control" placeholder="Account Number">
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select an Intended use</label>
                                                    <input type="text" name="" class="form-control" placeholder="Intended use">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>

                                            </div>
                                        </div>
                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="new-account-personal-info.php" class="common_btn next_btn black_btn">BACK</a> -->
                                                    <input type="butotn" class="common_btn next_btn black_btn previous" value="BACK">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="fine-print.php" class="common_btn next_btn green">NEXT STEP</a> -->
                                                    <input type="butotn" class="common_btn next_btn black_btn next" value="NEXT STEP">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                           
                        </fieldset>
                        
                        <fieldset> 
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 5</h3>
                                                    <p>THE FINE PRINT</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                             
                                                    <li class="active"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label_text">Your Tax Residency Details</label>
                                                    <p class="heading_text">
                                                        Tax residency is a method used by the Canada Revenue Agency (CRA) and by other countries to determine how much tax you should pay. It is your responsibility to determine your tax residency status in other countries.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you Tax Residant of Canada?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you US person for tax purpose?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">&nbsp;</label>
                                                    <input type="text" name="" class="form-control" placeholder="US Tax Identification Number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Are you US person for tax purpose?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Do you pay taxes in any countries?</label>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <select class="form-control">
                                                            <option disabled selected>Country</option>
                                                            <option>Country</option>
                                                            <option>Country</option>
                                                            <option>Country</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">&nbsp;</label>
                                                    <input type="text" name="" class="form-control" placeholder="Tax Identification Number">
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <h1 class="form_heading w-100">The Fine Print</h1>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p>By selecting Send application I declare, as required by Canadian law, that the tax residency information and U.S. Person status provided (including any Tax Identification Number) are, to the best of my knowledge and belief, correct and complete.</p>
                                                    <p>If any of this information changes, I will provide Habib Canadian Bank with the updated information within 30 days.</p>
                                                    <p>Failure to provide satisfactory self-certification of Tax residency or U.S. Person status may result in my account information being reported to the relevant tax authority and I may be subject to a penalty under the Income Tax Act.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="product-selection.php" class="common_btn next_btn black_btn">BACK</a> -->
                                                    <input type="button" class="common_btn next_btn black_btn previous" value="BACK" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <!-- <a href="" class="common_btn next_btn green">SUBMIT APPLICATION</a> -->
                                                    <input type="button" class="common_btn next_btn green next" value="SUBMIT APPLICATION">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                         
                        </fieldset>

                            </div>


                        </div>
                    </div>
                </div>  



            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body> 
    </html>