<?php  $page_class = "student_product_selection"; $page_bread = "<span>Product </span> Selection" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page new_account_creation">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1 class="flex-wrap"><?= $page_bread;?></h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new_account_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="new_account">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="steps d-flex">
                                                <div class="icons">
                                                    <img src="assets/images/step-one.png" alt="">
                                                </div>
                                                <div class="steps_heading ml-4">
                                                    <h3>STEP 4</h3>
                                                    <p>PRODUCT SELECTION</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="all_steps d-flex align-items-center">
                                                <ul class="default-list w-100">
                                                
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="new_account_forms">
                                    <form action="" class="commonForm">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select the type of account you wish to open</label>
                                                    <select class="form-control">
                                                        <option value="" disabled selected>Select An Account*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Account Holder Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select GIC Options</label>
                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select GIC Options*</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Financial Institution?</label>

                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>

                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Would you like to open Additional Accounts?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="label_text">Addional Account types?</label>
                                                    <div class="d-flex flex-wrap">
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked">
                                                                <label for='checked'>
                                                                    <p class="checkbox_text pl-2">Chequing Account</p>
                                                                </label>

                                                            </div>
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked4">
                                                                <label for='checked4'>
                                                                    <p class="checkbox_text pl-2">Savings Account</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked2">
                                                                <label for='checked2'>
                                                                    <p class="checkbox_text pl-2">GIC</p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl-0">
                                                            <div class="checkBox d-flex">
                                                                <input type="checkBox" name="" class="customChecbox" id="checked3">
                                                                <label for='checked3'>
                                                                    <p class="checkbox_text pl-2">Hi-Rate Savings Account </p>
                                                                </label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">GIC Options</label>
                                                    <input type="text" class="form-control" placeholder="Beneficiary Name">
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary active">
                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Yes
                                                        </label>
                                                        <label class="btn btn-secondary">
                                                            <input type="radio" name="options" id="option2" autocomplete="off"> No
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Financial Institutions to transfer from </label>

                                                    <select class="form-control" placeholder="">
                                                        <option value="" disabled selected>Select Financial Institutions</option>
                                                        <option value="">Male</option>
                                                        <option value="">Female</option>
                                                    </select>

                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Other</label>
                                                    <input type="text" name="" class="form-control" class="Other Institutions">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer from Another Financial Institution?</label>
                                                    <input type="text" name="" class="form-control" placeholder="Account Number">
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Select an Intended use</label>
                                                    <input type="text" name="" class="form-control" placeholder="Intended use">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Deposit Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label_text">Transfer Amount</label>
                                                    <input type="text" class="form-control" placeholder="Amount (CAD $)">
                                                    <p>Please Enter Value greater than or equal to 1000</p>
                                                </div>

                                            </div>
                                        </div>
                                        <hr class="line_break">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="student-travel-canada.php" class="common_btn next_btn black_btn">BACK</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="student-fine-print.php" class="common_btn next_btn green">NEXT STEP</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body>

    </html>