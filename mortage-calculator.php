<?php  $page_class = "mortage_calculator"; $page_bread = "<span>NEW ACCOUNT</span> APPLICATION" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page mortage_calculator_page"> 
               <div class="mortage_head">
               <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="mortal_heading">
                                <h1>MORTGAGE </h1>
                                <h2>CALCULATOR </h2>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="mortal_heading_text">
                                <p>Use our free mortgage calculator to estimate your monthly mortgage payments.
                                     We'll take care of everything else.</p>
                            </div>
                        </div>
                    </div>

                    <div class="price_calculator pt-2">
                           <form action="" class="commonForm">
                               <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Property price</label>
                                            <input type="text" placeholder="CAD 500, 000" class="form-control">
                                        </div>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="">Down payment</label>
                                                <label for="">CAD 100,000</label>
                                            </div>
                                            <input type="text" placeholder="CAD 500, 000" class="form-control">
                                            <p>A percentage of the home price paid up front and in cash. Usually at least 20%.</p>
                                        </div>
                                   </div>
                               </div>

                                     <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="">Loan duration</label>
                                                <label for="">25 years</label>
                                            </div>
                                            <div class="range-slider">
                                                <input class="range-slider__range" type="range" value="100" min="0" max="500">
                                                <!-- <span class="range-slider__value">0</span> -->
                                            </div>
                                           
                                        </div>
                                   </div>
                               </div>


                                    <div class="row calculate_interest">
                                   <div class="col-md-6">
                                   <label for="">Interest rate</label>
                                        <div class="form-group text-center"> 
                                                <input  type="text" class="form-control" placeholder="25.5%">
                                         </div>
                                       
                                            

                                          
                                        
                                           
                                        </div>
                                        <div class="col-md-6">
                                        <label for="" class="d-none d-md-block">&nbsp;</label>
                                        <div class="form-group"> 
                                            <a class="common_btn" href="javascript:void(0)">Calculate</a>
                                         </div>
                                        </div>
                                   </div>


                                  <div class="row mortage-stage">
                                    <div class="col-md-2">
                                      <div class="form-group"> 
                                      <label for="" class="d-md-block d-none">CD</label>
                                        <h1 class="text-center text-sm-normal"> <label for="" class="d-inline-block d-md-none">CD</label> 2,500</h1>
                                      </div>  
                                    </div>
                                    <div class="col-md-10">
                                      <label for="" class="d-none d-md-block">&nbsp;</label>
                                        <div class="form-group text-center"> 
                                          <p>Estimated monthly payment based on a CAD 400, 000 loan amount with a 2.25% fixed interest rate for the entire duration of the loan.</p>
                                        </div>
                                    </div>
                                  </div>


                    <div class="row">
                                   
                            <div class="col-md-12">
                            <div class="form-group">
                            <button class="next_btn grey common_btn">NEXT STEP</button>
                            </div>
                            </div>
                                   </div>


                               </div>
                           </form> 
                    </div>
                </div>
               </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body> 
    </html>