<?php  $page_class = "home_page"; ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Habib Canadian Bank </title>
      <?php include('includes/css.php'); ?>
   </head>
   <body>
      <!-- Header -->
      <?php include('includes/header.php'); ?>
      <!-- End Navigation Bar -->
 
    
        <section class="online_banking_page ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="online_banking_header">
                            <h1>ONLINE BANKING</h1>
                            <p>HCB Online Banking is easy, convenient and secure. Whether you are at home, the office, on vacation or on a business trip, HCB’s online facility gives you access to 24-hour banking.</p>
                            <p>Join our other customers already banking online, and benefit from:</p>
                            <ul class="default-list">
                           <li> <b>Convenience</b> – Pay your bills, check your balances, and transfer to and from your own HCB accounts, all from the convenience of your home or office.
                           </li>

 <li><b> Peace of mind</b> – We use the latest encryption technology to ensure your account information is protected at all times.</li>

 <li><b> Easy registration</b> – Simply ask your relationship manager for web banking at the time of opening your account. No additional forms to fill or sign. If you are already a customer and do not have online banking, simply call your branch and the relationship manager will help you register.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="hbl_mobile">
                            <img src="assets/images/online-banking-mobile.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="mobile_baking">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                    <div class="mobile_baking_heading text-center">
                        <h1>MOBILE BANKING</h1>
                        <p>Looking for on-the-go banking? You can do your banking, anywhere, anytime with the new HBZ Mobile Banking app. Simply download the HBZ app to your phone or tablet and you are set for mobile banking.</p>
                    </div>

                    </div>
             
                </div>
                <div class="banking_boxes_wraper">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="banking_boxes">
                                    <div class="bank_img_box">
                                        <img src="assets/images/hand.png" alt="">
                                    </div>
                                    <div class="bank_img_text">
                                        <h2>E-Statement</h2>
                                        <p>Receive your statement of accounts via secure email.</p>
                                    </div>
                                </div>
                            </div>

                                     <div class="col-md-4">
                                <div class="banking_boxes">
                                    <div class="bank_img_box">
                                        <img src="assets/images/chat.png" alt="">
                                    </div>
                                    <div class="bank_img_text">
                                        <h2>Account History</h2>
                                        <p>View & download real-time account history in either Excel /XML.</p>
                                    </div>
                                </div>
                            </div>

                                  <div class="col-md-4">
                                <div class="banking_boxes">
                                    <div class="bank_img_box">
                                        <img src="assets/images/authorize.png" alt="">
                                    </div>
                                    <div class="bank_img_text">
                                        <h2>Authenticated Swift Messages</h2>
                                        <p>View authenciated swift (remittance) messages.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
            </div>
        </section>
 
<div class="revised_saving_account">


<div class="ready-start">
                    <div class="ready_start_text">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <h1>READY TO GET STARTED?</h1>
                                </div>
                                <div class="col-md-4 text-md-right">
                                    <a href="javascript:void(0)" class="common_btn">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        <section class="online_more_banks left">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="online_more_side_img">
                             <h1>HCB</h1>
                             <p>Challenge Mechanism</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                    <div class="online_more_side_text">
                        <p>In order to further enhance security while logging onto HCBweb a “challenge” code has been introduced. A dynamically generated 5-digit challenge embedded in a graphic background is displayed whenever the HCBweb login screen appears on a user’s browser.</p>

                        <p>In addition to the Login ID, password and the optional secure key, the use has to enter the challenge digits displayed in the specified field. This feature enhances security as the challenge image is only visible to human eyes and given it refreshes every time the login screen appears, it prevents automated processes from guessing HCBweb passwords.</p>
                    </div>
                    </div>
                </div>
            </div>
        </section>

     <section class="online_more_banks right">
            <div class="container-fluid">
                <div class="row">
        
                    <div class="col-md-9">
                    <div class="online_more_side_text">
                       <p>HBZsecure Key is a unique security feature, developed by HCB that works in combination with existing HCBweb banking User Names and Passwords.</p>

<p>    It is a security key, provided on a Compact Disk (CD). Each CD is specially configured for the customer account with a Very Long Variable Password (VLVP). Once a client has this proprietary “Key”, they can access their accounts and make real time transfers, access other services, etc. from any computer that has Internet access.</p>

                    </div>
                    </div>
                    <div class="col-md-3">
                        <div class="online_more_side_img">
                             <h1>HCB</h1>
                             <p>Secure Key</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        

                <section class="online_more_banks side-three">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="online_more_side_img">
                             <h1>HCB</h1>
                             <p>Secure Key</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                    <div class="online_more_side_text">
                        <p>
                        An authentication process called HCBotp (One Time Password) for its HCBsecure Key users was introduced in order to further enhance security for HCBweb transactions.
                        </p>

<p> Every HCBweb subscriber currently using a HCBsecure Key must register either their mobile number or email ID. Upon logging-in with the HCBsecure Key, a “one time password” (HCBotp) is sent via SMS, email or both depending upon the subscription selection, for each session login. </p>

<p>    Each SMS or email will contain an OTP as well as a reference number for the individual session and will also be displayed on the screen. This will allow the user to match the OTP with the correct session. Each OTP will be valid for 5 minutes only.</p>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="ready_open_account">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ready_account_text">
                                    <h1>     Ready to <br> open an account? </h1>
                                    <a href="javascript:void">CLICK HERE</a>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="ready_men_icon">
                                    <img src="assets/images/men-icon.png" alt="">
                                    <p>BOOK A MEETING
                                        <br> WITH EXPERT.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      <!-- Footer -->
      <?php  include('includes/footer.php'); ?>

      <!-- End Footer -->
     <!-- Js Scripts -->
         <?php  include('includes/scripts.php'); ?>
      <!-- End Js Scripts -->
   </body>
</html>
