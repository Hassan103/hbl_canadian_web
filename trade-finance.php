<?php  $page_class = "trade-finance"; $page_bread = "<span>Trade</span> Finance" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page trade-finance-page ">
                <div class="heaader_inner about-us_page">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread;?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trade_finance_head">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="ittalic_head text-center mb-5 mt-5">
                                    <i>
                                    Achive International <span> bunsiness success </span>
                                    with right financial provider.
                                    </i>
                                </h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="gurantee_text">
                                    <p>As every commercial business is unique, we work with you to centralize your trade activities and design a suite of trade solutions geared to your underlying business flows. Our Relationship Managers and Trade specialists are here to share their expertise with you.</p>

                                    <ul class="default-list">
                                        <li>Global reach through the parent bank network and through the relationships with other major banks around the globe.</li>

                                        <li>Post-shipment financing</li>
                                        <li>Discounting of Documentary Letters of Credit and Documentary Collections.</li>
                                        <li>Issuance of Import Letters of Credit.</li>
                                        <li>Import financing to augment your working capital needs</li>
                                        <li>Invoice financing </li>
                                        <li>Help you arrange Receivable Insurance to protect your export and local receivables.</li>
                                    </ul>
                                </div>
                                <h5 class="mt-3 mb-3">Post-shipment finance:</h5>
                                <p>Upon presentation of the required shipping documents you can avail financing against Letters of Credit or Documentary Collection.</p>

                                <div class="gurantee_text pb-5">

                                    <ul class="default-list">
                                        <li>
                                            <h6>Documentary Letters of Credit</h6> Documentary Letters of Credits (LC) is a secure way of conducting international trade. The importer and exporter deal in confidence with one another and payment will be made if the transaction is carried out in accordance with the terms of the Letter of Credit, subject to the Uniform Customs and Practices for Documentary Credits (UCP).
                                        </li>
                                        <li>
                                            <h6>Documentary Collections</h6> Make and receive payments with your trusted trading partners using Documentary Collections. Using import and export documentary collection is a straightforward and cost-effective way to make international trade payments.
                                        </li>
                                        <li>
                                            <h6>Financing under Documentary Collections</h6> The bank provides financing by discounting your Collection documents to increase your working capital. Taking out receivable insurance against your buyers not only protects you from non-payment, but will also make financing more accessible.
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ready-start">
                    <div class="ready_start_text">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <h1>READY TO GET STARTED?</h1>
                                </div>
                                <div class="col-md-4 text-md-right">
                                    <a href="javascript:void(0)" class="common_btn btn_green-light">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="mt-3 pt-5">Bridge your capital needs with:</h5>
                            <div class="gurantee_text mb-5">

                                <ul class="default-list">
                                    <li>
                                        <h6>Import Financing (Trust Receipt)</h6> We provide short-term funding to bridge working capital gaps when there is an interval between paying suppliers and receiving payment from buyers.
                                    </li>

                                    <li>
                                        <h6>Invoice Financing </h6> Finance your local Accounts Receivables (provided they are insured by an acceptable Receivable Insurance company).
                                    </li>

                                    <li>
                                        <h6>Pre-shipment Finance</h6> Once you have a confirmed order from a buyer, backed by a documentary credit, we can provide you with secured working capital financing. This helps you produce and ship the goods, allowing you to take on new contracts and grow your business.
                                    </li>
                                    <li>
                                        <h6>Bank Guarantees and Bonds</h6> Bank guarantees and bonds support your ability to perform under a contract or meet payment obligations. Whether you require financial guarantees, bid bonds or performance bonds, Habib Canadian Bank can help you with these most widely used forms of guarantees and bonds. We also provide Standby Letters of Credit, which perform a similar function to bonds and guarantees.
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div> 
                <div class="ready_open_account">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ready_account_text">
                                    <h1>     Ready to <br> open an account? </h1>
                                    <a href="javascript:void">CLICK HERE</a>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="ready_men_icon">
                                    <img src="assets/images/men-icon.png" alt="">
                                    <p>BOOK A MEETING
                                        <br> WITH EXPERT.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->

                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
                    <!-- End Js Scripts -->
    </body> 
    </html>