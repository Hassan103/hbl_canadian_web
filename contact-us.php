<?php  $page_class = "contact_page"; $page_bread = "We Are Here To Help" ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Habib Canadian Bank </title>
        <?php include('includes/css.php'); ?>
    </head>

    <body>
        <!-- Header -->
        <?php include('includes/header.php'); ?>
            <!-- End Navigation Bar -->
            <div class="Inner_Page contact_page">
                <div class="heaader_inner contact_page_heading">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inner_main_heading">
                                    <h1><?= $page_bread; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="contact_option">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="contact_option_box">
                                   <div class="contact_icons"> <img src="assets/images/phone-icon.png" alt=""></div>
                                    <h4>CALL US</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact_option_box">
                                   <div class="contact_icons"> <img src="assets/images/calendar.png" alt=""></div>
                                    <h4 id="opointment_id">BOOK AN APPOINTMENT</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact_option_box">
                                   <div class="contact_icons"> <img src="assets/images/email-icon.png" alt=""></div>
                                    <h4>EMAIL US</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
          

                <section class="offices">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="offices_box">
                                    <h4>Head Office – Mississauga</h4>
                                    <p>
                                        <span>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        </span>
                                        <span class=""> 918 Dundas Street East,  </span>
                                        <span class="d-block d-md-inline-block"> Suite 1-B, Mississauga,</span>
                                        <span class="break"> Ontario, L4Y 4H9, Canada.</span>
                                    </p>

                                    <p>
                                        <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        </span>
                                        +1 (905) 276 5300
                                        <br>
                                        +1 (855) 82 HABIB
                                    </p>
                                    <p>
                                    <span><i class="fa fa-fax" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>

                                     <p>
                                     <span><i class="fa fa-envelope-open" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="offices_box">
                                    <h4>Scarborough</h4>
                                    <p>
                                        <span>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        </span>
                                        <span> 918 Dundas Street East,  </span>
                                        <span class="d-block d-md-inline-block">  Suite 1-B, Mississauga,</span>
                                        <span>Ontario, L4Y 4H9, Canada.</span>
                                    </p>

                                    <p>
                                        <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        </span>
                                        +1 (905) 276 5300
                                        <br>
                                        +1 (855) 82 HABIB
                                    </p>
                                    <p>
                                    <span><i class="fa fa-fax" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>

                                     <p>
                                     <span><i class="fa fa-envelope-open" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="offices_box offices_box_right">
                                    <h4>Brampton</h4>
                                       <p>
                                        <span>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        </span>
                                        <span> 918 Dundas Street East,  </span>
                                        <span class="d-block d-md-inline-block">  Suite 1-B, Mississauga,</span>
                                        <span>Ontario, L4Y 4H9, Canada.</span>
                                    </p>

                                    <p>
                                        <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        </span>
                                        +1 (905) 276 5300
                                        <br>
                                        +1 (855) 82 HABIB
                                    </p>
                                    <p>
                                    <span><i class="fa fa-fax" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>

                                     <p>
                                     <span><i class="fa fa-envelope-open" aria-hidden="true"></i></span>
                                        +1(905) 276 5400
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            

<section class="section_google_map">
<div style="width: 100%"><iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&height=600&hl=en&coord=43.605048, -79.595587&q=1065 Sierra Blvd, Mississauga, ON L4Y 2E3, Canada&ie=UTF8&t=&z=14&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/en/journey-planner.htm">Route Finder</a></iframe></div><br />
</section>



            <section class="related_links">
                <div class="container">
                     <div class="row">
                        <div class="col-md-12">
                        <div class="related_links_head">
                            <h2>Related Links</h2>

                        </div>
                        </div>
                     </div>
                    <div class="row">
                        
                        <div class="col-md-4">
                            <div class="related_links_box">
                                <a href="revised-saving-account.php" class="common_btn grey_btn">Savings</a>
                            </div>
                        </div>

                          <div class="col-md-4">
                            <div class="related_links_box">
                                <a href="lending.php" class="common_btn grey_btn">Lending</a>
                            </div>
                        </div>

                          <div class="col-md-4">
                            <div class="related_links_box">
                                <a href="trade-finance.php" class="common_btn grey_btn">Trade Finance</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
            <!-- Footer -->
            <?php  include('includes/footer.php'); ?>
                <!-- End Footer -->
                <!-- Js Scripts -->
                <?php  include('includes/scripts.php'); ?>
            <!-- End Js Scripts -->
    </body>

    </html>

