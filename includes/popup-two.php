
 <div class="custom_modal modal_two hide">
     <span class="close_btn">X</span>
    <div class="row">
        <div class="col-md-12">
            <div class="modal_text_section"> 
            <h1>BOOK AN</h1>
            <h2> APPOINTMENT</h2>
            </div>
        </div>
        </div>
<div class="row">
    <div class="col-md-12">
            <div class="modal_form_Section"> 
                <form action="" id="popup_form">
                <div class="d-flex flex-wrap">
                <div class="form-group col-md-4">
                        <label for="name">NAME</label>
                        <input type="text" name="name" placeholder="Please enter your name">
                    </div> 
                    <div class="form-group col-md-4">
                        <label for="name">Contact</label>
                        <input type="text" name="name" placeholder="Please enter your mobile number ">
                    </div>
                    <div class="form-group col-md-4">
                    	<label for="name">Select Branch</label>
                        <select>
                        	<option>British HBL</option>
                        	<option>British HBL</option>
                        	<option>British HBL</option>
                        	<option>British HBL</option>
                        	<option>British HBL</option>
                        </select>
                    </div>

					</div>
                   <div class="d-flex flex-wrap">
                   <div class="form-group col-md">
                        <label for="email">Email</label>
                        <input type="email" name="name" placeholder="Please enter your email address">
                    </div>


                    <div class="form-group col-md-3">
                        <label for="date">Date</label>
                        <input type="date" name="name" placeholder="Select Day">
                    </div>



                    <div class="form-group col-md">
                        <label for="time">Time</label>
                        <input type="text" name="name" placeholder="Choose time">
                    </div>

                   </div>
                    
                    
                    <div class="form-group col-md-12 text-center">
                        <button type="submit">
                           CONFIRM
                            <span><img src="assets/images/arrow.png" alt=""></span>
                        </button>
                    </div>
                </form>
            </div>
        </div> 
        </div>
  

</div>
<div class="bgOverly"></div>
<style>

.modal_two  h1{
    font-weight: 300;
    color: #fff; 
    margin-bottom: 0; 
    font-size: 32px;
}

.modal_two  h2{
    margin-bottom: 0; 
    color: #fff;
}
 .custom_modal {
    max-width: 700px;
    padding:8px;
	width: 700px;
	background: #167259;
	z-index: 99;
	border-radius: 15px;
	height:auto;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	position: fixed; 
	
}
 
.modal_two.active {
	top: 50%;
	visibility: visible;
	transition: ease all .6s
}

.modal_two.hide{
	visibility: hidden;
}

.modal_two .bgOverly {
	background: rgba(74, 211, 165, .9);
	position: fixed;
	left: 0;
	right: 0;
	bottom: 0;
	top: 0;
	width: 100%;
	height: 100%;
	display: none;
}

#popup_form label {
    display: block;
    background: #fff;
    width: 100%;
    margin-bottom: 0;
    color: #4a4a4a;
    font-size: 12px;
    padding-left: 15px;
    border-top-right-radius: 13px;
    border-top-left-radius: 13px;
    padding-top: 7px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}

#popup_form input, #popup_form select {
	border: 0;
	width: 100%;
	font-size: 10px;
	height: 35px;
	padding-left: 15px;
    border-bottom-right-radius: 12px;
    border-bottom-left-radius: 12px;	
    border-top-right-radius: 0;
	border-top-left-radius: 0;
	outline: none;
}

#popup_form input::placehoder, #popup_form select {
	color: #9b9b9b;
}


#popup_form input:focus {
	outline: none;
}

.modal_two   #popup_form button {
	color: #000;
	font-size: 16px;
	background: #64f0c1;
	height: 40px;
	border-radius: 12px;
	border: 0;
	width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 0 15px;
}

#popup_form button:hover {
	cursor: pointer;
}

.modal_text_section p {
	font-size: 12px;
	font-weight: 300;
	color: #fff;
	letter-spacing: 0.5px;
	margin-bottom: 0;
	padding-top: 8px;
}

.modal_text_section {
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding: 0 22px;
}

.modal_text_section h4 {
	font-size: 17px;
	color: #fff;
	line-height: 35px;
}

.custom_modal.modal_two.active {
    padding-top: 25px;
}

.modal_text_section span {
	font-size: 12px;
	font-weight: 300;
	color: #fff;
	letter-spacing: 0.5px;
	margin-bottom: 0
}

.modal_form_Section p {
	font-size: 15px;
	color: #fff;
	text-align: center;
	margin-bottom: 10px;
}

.modal_two .modal_form_Section {
	padding:0; 
    padding-top: 24px;
    
}
span.close_btn {
    position: absolute;
    right: 10px;
    background: #fff;
    border-radius: 51px;
    width: 15px;
    height: 15px;
    font-size: 10px;
    font-weight: 500;
    display: flex;
    justify-content: center;
    align-items: center;
    top: 7px;
    z-index: 999;
    
}

span.close_btn:hover {
    color:#fff;
    background:#4ad3a5;
    cursor:pointer;
    transition:ease all .4s;
}

@media(max-width: 768px){
 .custom_modal.modal_two {
    max-width: 100%;
    width: 83%;
    margin: 0 0;
}
    .modal_two h1{
            font-size: 22px;
    }
    .modal_two h2{
     font-size: 24px;   
    }
    .modal_two .modal_form_Section{
        padding-top: 15px
    }
}
</style>

 