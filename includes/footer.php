<!-- Subscribe & Update  -->
<div class="subscribe_section">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="subscribe_section_icons">
                    <img src="assets/images/2-Mail.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="subscribe_section_text">
                    <h4 id="call_pop">Subscribe and Stay update</h4>
                    <p>Get Exclusive deals, latest promotions and important information.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="subscribe_section_social">
                    <a href="https://www.facebook.com/HabibCanadianBank/">
                        <img src="assets/images/facebook.png">
                    </a>
                    <a href="https://www.linkedin.com/company/habib-canadian-bank/?originalSubdomain=ca">
                    <img src="assets/images/linkedin.png">
                    </a>
                        <a href="javascript:void(0)">
                    <img src="assets/images/instagram.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Subscribe & Update  -->

<footer class="footer_section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer_box">
                    <img src="assets/images/footer-logo-300x262.png" alt="">
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                <div class="col-md-6">
                       <div class="footer_box">
                    <h2>BANKING</h2>
                    <ul class="default-list">
                        <li><a href="javascript:void(0)">Personal</a></li>
                        <li><a href="javascript:void(0)">Business</a></li>
                        <li><a href="trade-finance.php">Trade Finance</a></li>
                        <li><a href="online-banking.php">HBZ Mobile</a></li>
                    </ul>
                </div>
                </div>
                            <div class="col-md-6">
                     <div class="footer_box">
                     <h2>SUPPORT</h2>
                    <ul class="default-list">
                        <li><a href="faq.php">FAQ's</a></li>
                        <li><a href="contat-us.php">Contact Us</a></li>
                        <li><a href="new-student-account-application.php">Student Application</a></li>
                        <li><a href="new-account-application.php">New Application</a></li>
                        <li><a href="javascript:void(0)">Voluntry Codes of Conduct</a></li>
                    </ul>
                </div>
            
                </div>
        
       
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="footer_box mt-3">
                            <h2>RESOURCE CENTER</h2>
                        <ul class="default-list">
                            <li><a href="javascript:void(0)">Privicy Notice</a></li>
                            <li><a href="javascript:void(0)">Schedule of Charges</a></li>
                            <li><a href="javascript:void(0)">Customer Complaints</a></li>
                        </ul>
                        <img src="assets/images/CDIC.png" class="img-fluid cdic-icon">
                        </div>
                    </div>
                    <div class="col-md-6">
                            <div class="footer_box mt-3">
                                <h2>ABOUT US</h2>
                                <ul class="default-list">
                                    <li><a href="about-us.php">About HCB</a></li>
                                    <li><a href="javascript:void(0)">Careers</a></li>
                                </ul>
                            </div>
                    </div>
                </div> 
            </div>
       

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="copy_right_section">
                    <p>Copyrights &#169; Habib Canadian Bank. All rights reserved.</p>
                    <ul>
                        <li><a href="privacy-security.php">Terms of Use</a></li>
                        <li><a href="javascript:void(0)">Legal </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php  include('includes/popup.php'); ?>
<?php  include('includes/popup-two.php'); ?>