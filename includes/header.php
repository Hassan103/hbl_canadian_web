  <header>
        <!-- Top Bar -->
        <div class="top_bar_wapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top_bar">
                            <ul class="default-list">
                                <li><a href="about-us.php">About</a></li>
                                <li><a href="our-team.php">Careers</a></li>
                                <li><a href="contact-us.php">Contact</a></li>
                                <li><a target="_blank" href="https://sub.habibbank.com/CAN/hPLUS">LOGIN</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Bar -->

        <!-- Navigation Bar -->
        <div class="MainNavigation">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-expand-md w-100">
                        <a class="navbar-brand" href="index.php">
                            <img src="assets/images/logo.jpg" alt="">
                        </a> 
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><path d="M3 7a1 1 0 100 2h24a1 1 0 100-2H3zm0 7a1 1 0 100 2h24a1 1 0 100-2H3zm0 7a1 1 0 100 2h24a1 1 0 100-2H3z"/></svg>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                            <ul class="navbar-nav mt-lg-0 align-items-stretch">
                                <li class="nav-item">
                                    <a class="nav-link cdicIcon" href="javascript:void(0)">
                                        <img src="assets/images/CDIC.png" alt="">
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="revised-saving-account.php">Savings <span class="sr-only"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="lending.php">Lending</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="trade-finance.php">Trade Finance</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
