
 <div class="custom_modal modal_one hide">
     <span class="close_btn">X</span>
    <div class="row">
        <div class="col-md-6">
            <div class="modal_text_section">
                <div class="modal_subscrib_icon">
                    <img src="assets/images/message-icon.png" alt="">
                    <p>Subscribe and stay updated!</p>
                </div>
                <h4>Get exclusive deals, latest promotions and important information</h4>

                <span>All this and more in the HBZ newsletter.</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="modal_form_Section">

                <p>Subscribe to our newsletter</p>
                <form action="" id="popup_form">
                    <div class="form-group">
                        <label for="name">NAME</label>
                        <input type="text" name="name" placeholder="Please enter your name">
                    </div>
                    <div class="form-group">
                        <label for="email">EMAIl</label>
                        <input type="email" name="name" placeholder="Please enter your email address">
                    </div>
                    <div class="form-group">
                        <button type="submit">
                            SUBSCRIBE
                            <span><img src="assets/images/arrow.png" alt=""></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div class="bgOverly"></div>
<style>
 .custom_modal {
	max-width: 700px;
	width: 700px;
	background: #167259;
	z-index: 99;
	border-radius: 15px;
 
	left: 50%;
	top: 35%;
	transform: translate(-50%, -50%);
	position: fixed; 
	
}
.modal_one.active {
	top: 50%;
	visibility: visible;
	transition: ease all .6s
}

.modal_one.hide{
	visibility: hidden;
}

.bgOverly {
	background: rgba(74, 211, 165, .9);
	position: fixed;
	left: 0;
	right: 0;
	bottom: 0;
	top: 0;
	width: 100%;
	height: 100%;
	display: none;
}

#popup_form label {
	display: block;
	background: #fff;
	width: 100%;
	margin-bottom: 0;
	color: #4a4a4a;
	font-size: 12px;
	padding-left: 15px;
	border-top-right-radius: 13px;
	border-top-left-radius: 13px;
	padding-top: 7px;
}

#popup_form input {
	border: 0;
	width: 100%;
	font-size: 10px;
	height: 35px;
	padding-left: 15px;
	border-bottom-right-radius: 12px;
	border-bottom-left-radius: 12px;
}

#popup_form input::placehoder {
	color: #9b9b9b;
}

#popup_form input:focus {
	outline: none;
}

#popup_form button {
	color: #000;
	font-size: 11px;
	background: #64f0c1;
	height: 40px;
	border-radius: 12px;
	border: 0;
	width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 0 15px;
}

#popup_form button:hover {
	cursor: pointer;
}

.modal_text_section p {
	font-size: 12px;
	font-weight: 300;
	color: #fff;
	letter-spacing: 0.5px;
	margin-bottom: 0;
	padding-top: 8px;
}

.modal_text_section {
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding: 0 22px;
}

.modal_text_section h4 {
	font-size: 17px;
	color: #fff;
	line-height: 35px;
}

.modal_text_section span {
	font-size: 12px;
	font-weight: 300;
	color: #fff;
	letter-spacing: 0.5px;
	margin-bottom: 0
}

.modal_form_Section p {
	font-size: 15px;
	color: #fff;
	text-align: center;
	margin-bottom: 10px;
}

.modal_form_Section {
	padding: 0 35px;
	padding-top: 24px;
}
span.close_btn {
    position: absolute;
    right: 10px;
    background: #fff;
    border-radius: 51px;
    width: 15px;
    height: 15px;
    font-size: 10px;
    font-weight: 500;
    display: flex;
    justify-content: center;
    align-items: center;
    top: 7px;
    z-index: 999;
    
}

span.close_btn:hover {
    color:#fff;
    background:#4ad3a5;
    cursor:pointer;
    transition:ease all .4s;
}

@media(max-width: 991px){
	.custom_modal.modal_one { 
    width: 90%; 
    border-radius: 15px; 
}
.modal_form_Section {
    padding: 0 10px;
    padding-top: 24px;
}
}
</style>

 